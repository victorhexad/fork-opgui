# opGUI Installation Guide

## Prerequisites

Before installing opGUI, please ensure you have the following prerequisites correctly installed:

1. **Node.js (Version 18.13.2):**

   - For Windows 11: Download from [here](https://nodejs.org/dist/v18.13.0/node-v18.13.0-x86.msi).

   - For Debian: Run the following commands in your terminal:

     ```bash
     curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash
     source ~/.profile
     nvm install 18.13.0
     ```

2. **NPM (Version 8.5.0):**

   - Included with Node.js when installing node.

3. **Yarn (Version 1.22.17):**

   - For Windows 11: Download from [here](https://github.com/yarnpkg/yarn/releases/download/v1.22.19/yarn-1.22.19.msi).

   - For Debian: Install using npm with the following command:

     ```bash
     npm install --global yarn
     ```

## Installation Steps

1. Clone this repository into your HOME folder. It is also mandatory to have opSimulation installed in your HOME directory.

## Windows:

1. Open a terminal and navigate to the project directory located in your home folder:

   ```bash
   cd %USERPROFILE%\opgui
   ```

2. Install the required dependencies using yarn:

   ```bash
   yarn install
   ```

3. Execute the Windows installation procedure:

   ```bash
   yarn win-install
   ```

4. To run the application, simply execute the following command:

   ```bash
   yarn win-launch
   ```

## Debian:

1. Open a terminal and navigate to the project directory located in your home folder:

   ```bash
   cd $HOME/opgui
   ```

2. Install the required dependencies using yarn:

   ```bash
   yarn install
   ```

3. Execute the Debian installation procedure:

   ```bash
   yarn deb-install
   ```

4. To run the application, simply execute the following command:

   ```bash
   yarn deb-launch
   ```

Please make sure to carefully follow the above steps to ensure a successful installation and execution of opGUI. If you encounter any issues or have any questions, do not hesitate to reach out to us for support. Happy coding!