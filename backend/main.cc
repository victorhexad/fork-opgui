/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 * opGUI - OpenAPI 3.0
 * The version of the OpenAPI document: 3.0.1
 *
 */


#include <QCommandLineOption>
#include <QCommandLineParser>
#include <QCoreApplication>
#include <QHostAddress>
#include <QRegExp>
#include <QStringList>
#include <QSharedPointer>
#include <QObject>
#include <QFile>

#include <thread>
#ifdef __linux__
#include <signal.h>
#include <unistd.h>
#endif

#include <OPGUICoreConfig.h>
#include <OPGUIInit.h>

#include <OAIApiRouter.h>
#include <qhttpengine/server.h>
#include <qhttpengine/qiodevicecopier.h>
#include <OAIApiRouterOP.h>
#include <qhttpengine/filesystemhandler.h>
#include <ServeIndexHtml.h>


#ifdef __linux__
void catchUnixSignals(QList<int> quitSignals) {
    auto handler = [](int sig) -> void {
        // blocking and not async-signal-safe func are valid
        qDebug() << "\nquit the application by signal " << sig;
        QCoreApplication::quit();
    };

    sigset_t blocking_mask;
    sigemptyset(&blocking_mask);
    for (auto sig : quitSignals)
        sigaddset(&blocking_mask, sig);

    struct sigaction sa;
    sa.sa_handler = handler;
    sa.sa_mask    = blocking_mask;
    sa.sa_flags   = 0;

    for (auto sig : quitSignals)
        sigaction(sig, &sa, nullptr);
}
#endif

int main(int argc, char * argv[])
{

    QCoreApplication a(argc, argv);
#ifdef __linux__
    QList<int> sigs({SIGQUIT, SIGINT, SIGTERM, SIGHUP});
    catchUnixSignals(sigs);
#endif

    BackendConfig& config = BackendConfig::getInstance();
    
    auto isApplicationReady = OPGUIInit::Initialise_OPGUI();
    if(!isApplicationReady)
    {
        OPGUIInit::ERROR_NOT_INITIALISED();
    }
    else
    {
        OPGUIInit::INFO_INITIALISED();
    }
    
    // Obtain the values
    QHostAddress address = QHostAddress(QString::fromStdString(config.BACKEND_IP));
    quint16 port = config.BACKEND_PORT;

    QHttpEngine::QObjectHandler rootHandler;
    ServeIndexHtml serveIndexHtml;
    rootHandler.registerMethod("", &serveIndexHtml, &ServeIndexHtml::serve);

    // Build the hierarchy of handlers
    QHttpEngine::FilesystemHandler fileHandler(QString::fromStdString(config.PATH_HOME) + "/opgui/backend/build/dist/assets");
    QHttpEngine::FilesystemHandler fileHandlerFonts(QString::fromStdString(config.PATH_HOME) + "/opgui/backend/build/dist/fonts");

    QSharedPointer<OpenAPI::OAIApiRequestHandler> restHandler(new OpenAPI::OAIApiRequestHandler());

    rootHandler.addSubHandler(QRegExp("assets/"), &fileHandler);
    rootHandler.addSubHandler(QRegExp("fonts/"), &fileHandlerFonts);
    rootHandler.addSubHandler(QRegExp("api/"), restHandler.data());

    QHttpEngine::Server server(&rootHandler);

    auto router = QSharedPointer<OAIApiRouterOP>::create();
    router->setUpRoutes();

    QObject::connect(restHandler.data(), &OpenAPI::OAIApiRequestHandler::requestReceived, [&](QHttpEngine::Socket *socket) {
        router->processRequest(socket);
    });

    qDebug() << "[INFO]Serving on " << address.toString() << ":" << port;
    // Attempt to listen on the specified port
    if (!server.listen(address, port)) {
        qCritical("[ERROR]Unable to listen on the specified port.");
        return 1;
    }

    int execResult = a.exec();

    return execResult;
}
