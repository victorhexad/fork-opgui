/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#ifndef TEST_HELPERS_H
#define TEST_HELPERS_H

#include <QRegularExpression>
#include <QFile>
#include <QTextStream>
#include <QFileInfo>
#include <QDomDocument>
#include <QDomElement>
#include <QDir>

namespace TestHelpers {

QString readFile(const QString &filePath) {
    QFile file(filePath);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return QString();

    QTextStream in(&file);
    return in.readAll();
}

QString removeSpacesBetweenTags(const QString& input) {
    QRegularExpression pattern(">(\\s+)<");
    QString output = input;
    output.replace(pattern, "><");
    return output;
}

bool createAndCheckFile(const QString& filenameWithPath, const QString& content) {
    QFile file(filenameWithPath);

    if (!file.open(QIODevice::WriteOnly)) {
        return false;
    }

    QTextStream stream(&file);
    stream << content;
    file.close();

    QFileInfo fileInfo(file);
    return fileInfo.exists() && fileInfo.isReadable();
}

QDomElement parseXMLString(const QString& xmlString) {
    QDomDocument doc;
    doc.setContent(xmlString);
    return doc.documentElement();
}

QString joinPaths(const QString& path1, const QString& path2) {
    QDir dir(path1);
    return QDir::cleanPath(path1 + '/' + path2);
}

} // namespace TestHelpers

#endif // TEST_HELPERS_H
