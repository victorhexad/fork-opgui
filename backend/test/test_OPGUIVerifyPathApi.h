/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

// Endpoint_test.cpp
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include <QDir>
#include <OPGUICore.h>
#include <OAIPathRequest.h>
#include <OAIVerifyPath_200_response.h>
#include "test_helpers.h"

class VERIFY_PATH_TEST : public ::testing::Test {
  public:
    BackendConfig &test_conf = BackendConfig::getInstance();
    QString workSpacePath;
    QString testDir;
    QString testDirFullPath;
    OpenAPI::OAIPathRequest request;
    OpenAPI::OAIVerifyPath_200_response response;
    
    void SetUp() override {
        //check workspace exist
        testDir = "test";
        workSpacePath = QString::fromStdString(test_conf.WORKSPACE);
        testDirFullPath = TestHelpers::joinPaths(workSpacePath,testDir);
        QDir dir(workSpacePath);
        ASSERT_TRUE(dir.exists());
        ASSERT_TRUE(dir.mkpath(testDirFullPath));
    }

    void TearDown() override {
        QDir dir(testDirFullPath);
        if(dir.exists()){
            ASSERT_TRUE(dir.removeRecursively());
        }
    }
};

TEST_F(VERIFY_PATH_TEST, Verify_Path_Empty_Folder_Exists_Positive) {
    this->request.setPath(this->testDir);

    bool result = OPGUICore::api_verify_path(this->request, this->response);

    EXPECT_TRUE(result);
    EXPECT_EQ(this->response.getRealPath(), TestHelpers::joinPaths(this->workSpacePath,this->request.getPath()));
    EXPECT_TRUE(this->response.isOk());
    EXPECT_TRUE(this->response.isEmpty());
}

TEST_F(VERIFY_PATH_TEST, Verify_Path_Folder_Containing_Folder_Exists_Positive) {
    this->request.setPath(this->testDir);

    QString subfolder = "test_sub";
    
    QDir dir(this->testDirFullPath);
    
    ASSERT_TRUE(dir.mkpath(TestHelpers::joinPaths(this->testDirFullPath,subfolder)));

    bool result = OPGUICore::api_verify_path(this->request, this->response);

    EXPECT_TRUE(result);
    EXPECT_EQ(this->response.getRealPath(), TestHelpers::joinPaths(this->workSpacePath,this->request.getPath()));
    EXPECT_TRUE(this->response.isOk());
    EXPECT_FALSE(this->response.isEmpty());
}

TEST_F(VERIFY_PATH_TEST, Verify_Path_Folder_Containing_File_Exists_Positive) {
    this->request.setPath(this->testDir);

    QString filename = "test_file.xml";
    
    QFile file(TestHelpers::joinPaths(this->testDirFullPath,filename));
    ASSERT_TRUE(file.open(QIODevice::WriteOnly));
    file.close();

    bool result = OPGUICore::api_verify_path(this->request, this->response);

    EXPECT_TRUE(result);
    EXPECT_EQ(this->response.getRealPath(),TestHelpers::joinPaths(this->workSpacePath,this->request.getPath()));
    EXPECT_TRUE(this->response.isOk());
    EXPECT_FALSE(this->response.isEmpty());
}

TEST_F(VERIFY_PATH_TEST, Verify_Path_Folder_Containing_File_Searched_File_Positive) {
    this->request.setPath("test_file.xml");

    QString filename = "test_file.xml";
    
    QFile file(TestHelpers::joinPaths(this->testDirFullPath,filename));
    ASSERT_TRUE(file.open(QIODevice::WriteOnly));
    file.close();

    bool result = OPGUICore::api_verify_path(this->request, this->response);

    EXPECT_TRUE(result);
    EXPECT_EQ(this->response.getRealPath(),TestHelpers::joinPaths(this->testDirFullPath,this->request.getPath()));
    EXPECT_TRUE(this->response.isOk());
    EXPECT_FALSE(this->response.isEmpty());
}

TEST_F(VERIFY_PATH_TEST, Verify_Path_SubFolder_Containing_File_Searched_File_Positive) {
    this->request.setPath("test_file.xml");

    QString subfolder = "test_sub";
    QDir dir(this->testDirFullPath);
    ASSERT_TRUE(dir.mkpath(TestHelpers::joinPaths(this->testDirFullPath,subfolder)));

    QString filename = "test_file.xml";
    QFile file(TestHelpers::joinPaths(TestHelpers::joinPaths(this->testDirFullPath,"test_sub"),filename));
    ASSERT_TRUE(file.open(QIODevice::WriteOnly));
    file.close();

    bool result = OPGUICore::api_verify_path(this->request, this->response);

    EXPECT_TRUE(result);
    EXPECT_EQ(this->response.getRealPath(),TestHelpers::joinPaths(TestHelpers::joinPaths(this->testDirFullPath,"test_sub"),this->request.getPath()));
    EXPECT_TRUE(this->response.isOk());
    EXPECT_FALSE(this->response.isEmpty());
}

TEST_F(VERIFY_PATH_TEST, Verify_path_two_folders_with_same_name_in_different_levels_Negative) {
    this->request.setPath(this->testDir);

    QString subfolder = "test";
    
    QDir dir(this->testDirFullPath);
    
    ASSERT_TRUE(dir.mkpath(TestHelpers::joinPaths(this->testDirFullPath,subfolder)));

    bool result = OPGUICore::api_verify_path(this->request, this->response);

    EXPECT_TRUE(result);
    EXPECT_EQ(this->response.getRealPath(), "Folder/File found more than once inside the workspace directory or subdirectories");
    EXPECT_FALSE(this->response.isOk());
    EXPECT_TRUE(this->response.isEmpty());
}

TEST_F(VERIFY_PATH_TEST, Verify_Path_Folder_Exists_Negative) {
    QString subfolder = "test_not_exist";
    request.setPath(subfolder);

    bool result = OPGUICore::api_verify_path(this->request, this->response);

    EXPECT_TRUE(result);
    EXPECT_FALSE(this->response.isOk());
    EXPECT_TRUE(this->response.isEmpty());
}


