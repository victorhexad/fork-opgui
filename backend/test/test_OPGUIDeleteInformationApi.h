/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include <QDir>
#include <OPGUICore.h>
#include <OAIPathRequest.h>
#include "OAIDefault200Response.h"
#include "test_helpers.h"

class DELETE_INFORMATION_TEST : public ::testing::Test {
  public:
    BackendConfig &test_conf = BackendConfig::getInstance();
    QString workSpacePath;
    QString testDir;
    QString testDirFullPath;
    OpenAPI::OAIPathRequest request;
    OpenAPI::OAIDefault200Response response;
    OpenAPI::OAIDefault200Response response200Expected;
    
    void SetUp() override {
        //check workspace exist
        response200Expected.setResponse("true");
        testDir = "test";
        workSpacePath = QString::fromStdString(test_conf.WORKSPACE);
        testDirFullPath = TestHelpers::joinPaths(workSpacePath,testDir);
        QDir dir(workSpacePath);
        ASSERT_TRUE(dir.exists());
        ASSERT_TRUE(dir.mkpath(testDirFullPath));
    }

    void TearDown() override {
        QDir dir(testDirFullPath);
        if(dir.exists()){
            ASSERT_TRUE(dir.removeRecursively());
        }
    }
};

TEST_F(DELETE_INFORMATION_TEST, Delete_Info_Containing_Folders_Positive) {
    QString subfolder1 = "test_sub1";
    QString subfolder2 = "test_sub2";
    this->request.setPath(this->testDirFullPath);

    QDir dir(this->testDirFullPath);
    ASSERT_TRUE(dir.mkpath(TestHelpers::joinPaths(this->testDirFullPath,subfolder1)));
    ASSERT_TRUE(dir.mkpath(TestHelpers::joinPaths(this->testDirFullPath,subfolder2)));

    bool result = OPGUICore::api_delete_information(this->request, this->response);

    EXPECT_TRUE(result);
    EXPECT_EQ(this->response.getResponse(),this->response200Expected.getResponse()) << "Test: Delete information function did not return expected response";
    
    ASSERT_TRUE(dir.entryList(QDir::NoDotAndDotDot | QDir::AllEntries).isEmpty());
}

TEST_F(DELETE_INFORMATION_TEST, Delete_Info_Containing_Files_Positive) {
    QString file1Name = "test_file1.xml";
    QString file2Name = "test_file2.txt";
    this->request.setPath(this->testDirFullPath);

    QDir dir(this->testDirFullPath);

    QFile file1(dir.filePath(file1Name));
    ASSERT_TRUE(file1.open(QIODevice::WriteOnly));
    file1.close();

    QFile file2(dir.filePath(file2Name));
    ASSERT_TRUE(file2.open(QIODevice::WriteOnly));
    file2.close();

    bool result = OPGUICore::api_delete_information(this->request, this->response);

    EXPECT_TRUE(result);
    EXPECT_EQ(this->response.getResponse(),this->response200Expected.getResponse()) << "Test: Delete information function did not return expected response";

    ASSERT_TRUE(dir.entryList(QDir::NoDotAndDotDot | QDir::AllEntries).isEmpty());
}

TEST_F(DELETE_INFORMATION_TEST, Delete_Info_Containing_Folders_and_Files_Positive) {
    QString subfolder = "test_sub1";
    QString fileName = "test_file.xml";
    this->request.setPath(this->testDirFullPath);

    QDir dir(this->testDirFullPath);
    ASSERT_TRUE(dir.mkpath(TestHelpers::joinPaths(this->testDirFullPath,subfolder)));

    QFile file(dir.filePath(fileName));
    ASSERT_TRUE(file.open(QIODevice::WriteOnly));
    file.close();

    bool result = OPGUICore::api_delete_information(this->request, this->response);

    EXPECT_TRUE(result);
    EXPECT_EQ(this->response.getResponse(),this->response200Expected.getResponse()) << "Test: Delete information function did not return expected response";

    ASSERT_TRUE(dir.entryList(QDir::NoDotAndDotDot | QDir::AllEntries).isEmpty());
}

TEST_F(DELETE_INFORMATION_TEST, Delete_Info_Empty_Folder_Positive) {
    this->request.setPath(this->testDirFullPath);

    QDir dir(this->testDirFullPath);

    bool result = OPGUICore::api_delete_information(this->request, this->response);

    EXPECT_TRUE(result);
    EXPECT_EQ(this->response.getResponse(),this->response200Expected.getResponse()) << "Test: Delete information function did not return expected response";

    ASSERT_TRUE(dir.entryList(QDir::NoDotAndDotDot | QDir::AllEntries).isEmpty());
}

TEST_F(DELETE_INFORMATION_TEST, Delete_Non_Existing_Folder_Negative) {
    this->request.setPath("not_existing_wrong_path");

    bool result = OPGUICore::api_delete_information(this->request, this->response);

    EXPECT_FALSE(result);
}


