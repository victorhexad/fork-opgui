/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include "gtest/gtest.h"
#include "OAIOpSimulationManagerXmlRequest.h"
#include "OAISimulationConfig.h"
#include "OAIDefault200Response.h"
#include <OPGUICore.h>
#include <OPGUICoreConfig.h>
#include "test_helpers.h"

QString expectedXmlExample1 = QString::fromStdString(
        R"(<?xml version="1.0" encoding="UTF-8"?>
        <opSimulationManager>
            <logLevel>2</logLevel>
            <simulation>opSimulation</simulation>
            <logFileSimulationManager>$$CORE_DIR$$opSimulationManager.log</logFileSimulationManager>
            <libraries>modules</libraries>
            <simulationConfigs>
                <simulationConfig>
                    <configurations>$$CORE_DIR$$configurations</configurations>
                    <logFileSimulation>$$CORE_DIR$$logFileSimulation.log</logFileSimulation>
                    <results>$$CORE_DIR$$results</results>
                </simulationConfig>
            </simulationConfigs>
        </opSimulationManager>)"
);

OpenAPI::OAIOpSimulationManagerXmlRequest getRequestExample1(QString &coreDir) {

        OpenAPI::OAIOpSimulationManagerXmlRequest request;
        request.setLogLevel(2);
        request.setLogFileSimulationManager("opSimulationManager.log");
        request.setSimulation("opSimulation");
        request.setLibraries("modules");

        OpenAPI::OAISimulationConfig simConfig;
        simConfig.setNumberOfExecutions(5);
        simConfig.setLogFileSimulation(coreDir+"logFileSimulation.log");
        simConfig.setConfigurations(coreDir+"configurations");
        simConfig.setResults(coreDir+"results");

        QList<OpenAPI::OAISimulationConfig> configs;
        configs.append(simConfig);
        request.setSimulationConfigs(configs);

        return request;
}

// Example data
QString expectedXmlExample2 = QString::fromStdString(
        R"(<?xml version="1.0" encoding="UTF-8"?>
        <opSimulationManager>
        <logLevel>2</logLevel>
        <simulation>opSimulation</simulation>
        <logFileSimulationManager>$$CORE_DIR$$opSimulationManager.log</logFileSimulationManager>
        <libraries>modules</libraries>
            <simulationConfigs>
                <simulationConfig>
                    <configurations>$$CORE_DIR$$configs1</configurations>
                    <logFileSimulation>$$CORE_DIR$$opSimulation1.log</logFileSimulation>
                    <results>$$CORE_DIR$$results1</results>
                </simulationConfig>
                <simulationConfig>
                    <configurations>$$CORE_DIR$$configs2</configurations>
                    <logFileSimulation>$$CORE_DIR$$opSimulation2.log</logFileSimulation>
                    <results>$$CORE_DIR$$results2</results>
                </simulationConfig>
            </simulationConfigs>
        </opSimulationManager>)"
);

OpenAPI::OAIOpSimulationManagerXmlRequest getRequestExample2(QString &coreDir) {
        OpenAPI::OAIOpSimulationManagerXmlRequest request;
        request.setLogLevel(2);
        request.setLogFileSimulationManager("opSimulationManager.log");
        request.setSimulation("opSimulation");
        request.setLibraries("modules");

        OpenAPI::OAISimulationConfig simConfig1;
        simConfig1.setNumberOfExecutions(1);
        simConfig1.setLogFileSimulation(coreDir+"opSimulation1.log");
        simConfig1.setConfigurations(coreDir+"configs1");
        simConfig1.setResults(coreDir+"results1");

        OpenAPI::OAISimulationConfig simConfig2;
        simConfig2.setNumberOfExecutions(2);
        simConfig2.setLogFileSimulation(coreDir+"opSimulation2.log");
        simConfig2.setConfigurations(coreDir+"configs2");
        simConfig2.setResults(coreDir+"results2");

        QList<OpenAPI::OAISimulationConfig> configs;
        configs.append(simConfig1);
        configs.append(simConfig2);
        request.setSimulationConfigs(configs);

        return request;
}

class XML_EXPORT_TEST : public ::testing::Test {
  public:
    BackendConfig &test_conf = BackendConfig::getInstance();
    QString openPassCoreDir;
    QString configFileFullPath;
    QString backupFilePath;
    OpenAPI::OAIOpSimulationManagerXmlRequest request;
    OpenAPI::OAIDefault200Response response;
    OpenAPI::OAIDefault200Response response200Expected;
    
    void SetUp() override {
        response200Expected.setResponse("opsimulation manager data exported correctly!");
        // Prepare the file paths
        this->openPassCoreDir = QString::fromStdString(test_conf.PATH_OPENPASS_CORE);
        this->openPassCoreDir = (openPassCoreDir.endsWith("/") ? this->openPassCoreDir : this->openPassCoreDir + "/");

        this->configFileFullPath =  TestHelpers::joinPaths(openPassCoreDir,QString::fromStdString(test_conf.OPSIMULATION_MANAGER_XML));
        this->backupFilePath = this->configFileFullPath + QString::fromStdString("_bk");

        //check core path exist
        QDir dir(this->openPassCoreDir);
        ASSERT_TRUE(dir.exists());

        // Check if the file already exists and if yes, rename it
        QFile file(this->configFileFullPath);
        bool fileExists = file.exists();
        if (fileExists) {
            bool fileRenamed = file.rename(backupFilePath);
            EXPECT_TRUE(fileRenamed) << "SetUp: Failed to rename config file:"+configFileFullPath.toStdString();
        }
    }

    void TearDown() override {
        // Delete the newly created file
        bool removed = QFile::remove(configFileFullPath);
        EXPECT_TRUE(removed) << "TearDown: Failed to remove the newly created file";

        // Rename the backup file back to its original name
        QFile backupFile(this->backupFilePath);
        if (backupFile.exists()) {
            bool fileRenamed = backupFile.rename(this->configFileFullPath);
            EXPECT_TRUE(fileRenamed) << "SetUp: Failed to rename the backuped config file";
        }
    }
};

TEST_F(XML_EXPORT_TEST, TestExportToXML_OneValueInArray_Positive) {
    // Call XML export function
    this->request = getRequestExample1(this->openPassCoreDir);
    bool result = OPGUICore::api_export_opSimulationManager_xml(request,response);  // Replace with export to XML function called...
    
    // Check that the XML file has been created
    EXPECT_TRUE(result) << "Test: exportToXML function returned failed";
    EXPECT_EQ(this->response.getResponse(),this->response200Expected.getResponse()) << "Test: exportToXML function did not return expected";
    EXPECT_TRUE(QFile::exists(this->configFileFullPath)) << "Test: exportToXML did not created any file";

    // Read the content of the generated file
    QString actualXml = TestHelpers::readFile(this->configFileFullPath);

    expectedXmlExample1.replace("$$CORE_DIR$$", this->openPassCoreDir);
    EXPECT_EQ(TestHelpers::removeSpacesBetweenTags(expectedXmlExample1.simplified()),TestHelpers::removeSpacesBetweenTags(actualXml.simplified()))<<
    "Test: contents of expected and generated xml file differ";
}

TEST_F(XML_EXPORT_TEST, TestExportToXML_TwoValuesInArray_Positive) {
    // Call XML export function
    this->request = getRequestExample2(this->openPassCoreDir);
    bool result = OPGUICore::api_export_opSimulationManager_xml(request,response);  // Replace with export to XML function called...
    
    // Check that the XML file has been created
    EXPECT_TRUE(result) << "Test: exportToXML function returned failed";
    EXPECT_EQ(this->response.getResponse(),this->response200Expected.getResponse()) << "Test: exportToXML function did not return expected";
    EXPECT_TRUE(QFile::exists(this->configFileFullPath)) << "Test: exportToXML did not created any file";

    // Read the content of the generated file
    QString actualXml = TestHelpers::readFile(this->configFileFullPath);

    expectedXmlExample2.replace("$$CORE_DIR$$", this->openPassCoreDir);


    qDebug()<<TestHelpers::removeSpacesBetweenTags(expectedXmlExample2.simplified());
    qDebug()<<TestHelpers::removeSpacesBetweenTags(actualXml.simplified());
    EXPECT_EQ(TestHelpers::removeSpacesBetweenTags(expectedXmlExample2.simplified()),TestHelpers::removeSpacesBetweenTags(actualXml.simplified()))<<
     "Test: contents of expected and generated xml file differ";
}

TEST_F(XML_EXPORT_TEST, TestExportToXML_DifferentObjectToCompare_Negative) {
    // Retrieve the test parameters
    this->request = getRequestExample1(this->openPassCoreDir);

    // Call XML export function
    bool result = OPGUICore::api_export_opSimulationManager_xml(request,response);  // Replace with export to XML function called...
    
    // Check that the XML file has been created
    EXPECT_TRUE(result) << "Test: exportToXML function returned failed";
    EXPECT_TRUE(QFile::exists(this->configFileFullPath)) << "Test: exportToXML did not created any file";

    // Read the content of the generated file
    QString actualXml = TestHelpers::readFile(this->configFileFullPath);

    EXPECT_NE(TestHelpers::removeSpacesBetweenTags(expectedXmlExample2.simplified()),TestHelpers::removeSpacesBetweenTags(actualXml.simplified()))<<
     "Test: contents of expected and generated xml file differ";
}

TEST_F(XML_EXPORT_TEST, TestExportToXML_DifferentObjectToCompare_Negative_2) {
    // Retrieve the test parameters
    this->request = getRequestExample2(this->openPassCoreDir);

    // Call XML export function
    bool result = OPGUICore::api_export_opSimulationManager_xml(request,response);  // Replace with export to XML function called...
    //bool result = true;
    
    // Check that the XML file has been created
    EXPECT_TRUE(result) << "Test: exportToXML function returned failed";
    EXPECT_TRUE(QFile::exists(this->configFileFullPath)) << "Test: exportToXML did not created any file";

    // Read the content of the generated file
    QString actualXml = TestHelpers::readFile(this->configFileFullPath);

    EXPECT_NE(TestHelpers::removeSpacesBetweenTags(expectedXmlExample1.simplified()),TestHelpers::removeSpacesBetweenTags(actualXml.simplified()))<<
     "Test: contents of expected and generated xml file differ";
}


