/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include "test_OPGUIExportOpsimulationManagerXmlApi.h"
#include "test_OPGUIVerifyPathApi.h"
#include "test_OPGUIDeleteInformationApi.h"
#include "test_OPGUIRunOpSimulationManagerApi.h"


int main(int argc, char * argv[])
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}