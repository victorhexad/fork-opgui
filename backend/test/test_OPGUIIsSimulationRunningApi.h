/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include <QDir>
#include <QThread>
#include <OPGUICore.h>
#include <OAIPathRequest.h>
#include "OAIDefault200Response.h"
#include "test_helpers.h"
#include <QRegularExpression>

QString configXmlSimulationShort = QString::fromStdString(
        R"(<?xml version="1.0" encoding="UTF-8"?>
        <opSimulationManager>
            <logLevel>5</logLevel>
            <simulation>opSimulation</simulation>
            <logFileSimulationManager>$$CORE_DIR$$opSimulationManager.log</logFileSimulationManager>
            <libraries>modules</libraries>
            <simulationConfigs>
                <simulationConfig>
                    <configurations>$$CORE_DIR$$configs</configurations>
                    <logFileSimulation>$$CORE_DIR$$opSimulation1.log</logFileSimulation>
                    <results>$$CORE_DIR$$results_TEST_TEMP_2</results>
                </simulationConfig>
                <simulationConfig>
                    <configurations>$$CORE_DIR$$configs</configurations>
                    <logFileSimulation>$$CORE_DIR$$opSimulation2.log</logFileSimulation>
                    <results>$$CORE_DIR$$results2</results>
                </simulationConfig>
                <simulationConfig>
                    <configurations>$$CORE_DIR$$configs</configurations>
                    <logFileSimulation>$$CORE_DIR$$opSimulation3.log</logFileSimulation>
                    <results>$$CORE_DIR$$results3</results>
                </simulationConfig>
            </simulationConfigs>
        </opSimulationManager>)"
);

QString configXmlSimulationLong = QString::fromStdString(
         R"(<?xml version="1.0" encoding="UTF-8"?>
        <opSimulationManager>
            <logLevel>5</logLevel>
            <simulation>opSimulation</simulation>
            <logFileSimulationManager>$$CORE_DIR$$opSimulationManager.log</logFileSimulationManager>
            <libraries>modules</libraries>
            <simulationConfigs>
                <simulationConfig>
                    <configurations>$$CORE_DIR$$configs</configurations>
                    <logFileSimulation>$$CORE_DIR$$opSimulation1_TEST_TEMP.log</logFileSimulation>
                    <results>$$CORE_DIR$$results_TEST_TEMP_2</results>
                </simulationConfig>
                <simulationConfig>
                    <configurations>$$CORE_DIR$$configs</configurations>
                    <logFileSimulation>$$CORE_DIR$$opSimulation2_TEST_TEMP.log</logFileSimulation>
                    <results>$$CORE_DIR$$results2</results>
                </simulationConfig>
                <simulationConfig>
                    <configurations>$$CORE_DIR$$configs</configurations>
                    <logFileSimulation>$$CORE_DIR$$opSimulation3_TEST_TEMP.log</logFileSimulation>
                    <results>$$CORE_DIR$$results3</results>
                </simulationConfig>
                <simulationConfig>
                    <configurations>$$CORE_DIR$$configs</configurations>
                    <logFileSimulation>$$CORE_DIR$$opSimulation1_TEST_TEMP.log</logFileSimulation>
                    <results>$$CORE_DIR$$results_TEST_TEMP_2</results>
                </simulationConfig>
                <simulationConfig>
                    <configurations>$$CORE_DIR$$configs</configurations>
                    <logFileSimulation>$$CORE_DIR$$opSimulation2_TEST_TEMP.log</logFileSimulation>
                    <results>$$CORE_DIR$$results2</results>
                </simulationConfig>
                <simulationConfig>
                    <configurations>$$CORE_DIR$$configs</configurations>
                    <logFileSimulation>$$CORE_DIR$$opSimulation3_TEST_TEMP.log</logFileSimulation>
                    <results>$$CORE_DIR$$results3</results>
                </simulationConfig>
                <simulationConfig>
                    <configurations>$$CORE_DIR$$configs</configurations>
                    <logFileSimulation>$$CORE_DIR$$opSimulation1_TEST_TEMP.log</logFileSimulation>
                    <results>$$CORE_DIR$$results_TEST_TEMP_2</results>
                </simulationConfig>
                <simulationConfig>
                    <configurations>$$CORE_DIR$$configs</configurations>
                    <logFileSimulation>$$CORE_DIR$$opSimulation2_TEST_TEMP.log</logFileSimulation>
                    <results>$$CORE_DIR$$results2</results>
                </simulationConfig>
                <simulationConfig>
                    <configurations>$$CORE_DIR$$configs</configurations>
                    <logFileSimulation>$$CORE_DIR$$opSimulation3_TEST_TEMP.log</logFileSimulation>
                    <results>$$CORE_DIR$$results3</results>
                </simulationConfig>
            </simulationConfigs>
        </opSimulationManager>)"
);

//search ERROR

class IS_SIMULATION_RUNNING_TEST : public ::testing::Test {
  public:
    BackendConfig &test_conf = BackendConfig::getInstance();
    int tryCount;
    QString openPassCoreDir;
    QString configFileFullPath;
    QString backupFilePath;

    OpenAPI::OAIBoolean200Response response;
    OpenAPI::OAIDefault200Response response200Expected;
    OpenAPI::OAIDefault200Response responseRunSim;
    
    void SetUp() override {
        response200Expected.setResponse("Simulation started correctly");
        // Prepare the file paths
        this->openPassCoreDir = QString::fromStdString(test_conf.PATH_OPENPASS_CORE);
        this->openPassCoreDir = (openPassCoreDir.endsWith("/") ? this->openPassCoreDir : this->openPassCoreDir + "/");

        this->configFileFullPath =  TestHelpers::joinPaths(this->openPassCoreDir,QString::fromStdString(test_conf.OPSIMULATION_MANAGER_XML));
        this->backupFilePath = this->configFileFullPath + QString::fromStdString("_bk");

        //check core path exist
        QDir dir(this->openPassCoreDir);
        ASSERT_TRUE(dir.exists())<< "SetUp: PATH_OPENPASS_CORE not existing:"+this->openPassCoreDir.toStdString();

        // Check if the file already exists and if yes, rename it
        QFile file(this->configFileFullPath);
        bool fileExists = file.exists();
        if (fileExists) {
            bool fileRenamed = file.rename(this->backupFilePath);
            ASSERT_TRUE(fileRenamed) << "SetUp: Failed to rename config file:"+configFileFullPath.toStdString();
        }
    }
    
    void TearDown() override {
        
        QThread::sleep(2);
        // Delete the newly created file
        bool removed = QFile::remove(this->configFileFullPath);
        ASSERT_TRUE(removed) << "TearDown: Failed to remove the test config file file:" + this->configFileFullPath.toStdString();

        QDir dir(this->openPassCoreDir);
        foreach(QString file, dir.entryList(QDir::Files)) {
            QString filePath = dir.absoluteFilePath(file);
            if (filePath.contains("TEST_TEMP")) {
                removed = QFile::remove(filePath);
                ASSERT_TRUE(removed) << "TearDown: Failed to remove the test file:" + filePath.toStdString();
            }
        }

        foreach(QString directory, dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot)) {
            QString innerDir = dir.absoluteFilePath(directory);
            if (innerDir.contains("TEST_TEMP")) {
                removed = QDir(innerDir).removeRecursively();
                ASSERT_TRUE(removed) << "TearDown: Failed to recursively remove the folder:" + innerDir.toStdString();
            }
        }

        // Rename the backup file back to its original name
        QFile backupFile(this->backupFilePath);
        if (backupFile.exists()) {
            bool fileRenamed = backupFile.rename(this->configFileFullPath);
            ASSERT_TRUE(fileRenamed) << "SetUp: Failed to rename the backuped config file";
        }
        
    }
};

TEST_F(IS_SIMULATION_RUNNING_TEST, is_simulation_running_short_simulation_positive) {
    //QThread::sleep(10);
    // Create XML config file
    this->tryCount=1;
    configXmlSimulationShort.replace("$$CORE_DIR$$", this->openPassCoreDir);
    bool fileCreated = TestHelpers::createAndCheckFile(this->configFileFullPath,configXmlSimulationShort);
    ASSERT_TRUE(fileCreated) << "Test: test sim config file could not be created";

    bool result = OPGUICore::api_run_opSimulationManager(this->responseRunSim); 

    // Check that the response is ok
    ASSERT_TRUE(result) << "Test: runOpSimulationManager returned failed";
    ASSERT_EQ(this->responseRunSim.getResponse(),this->response200Expected.getResponse()) << "Test: run op simulation function did not return expected 200 response";

    //Wait 1 sec before first try
    QThread::sleep(2);

    while(this->tryCount>0) {
        QThread::sleep(1);
        result = OPGUICore::api_is_simulation_running(this->response); 
        ASSERT_TRUE(result) << "Test: isSimulation running failed";
        //expect is running true
        ASSERT_EQ(this->response.isResponse(),true) << "Test: isRunning was expected to return true";
        this->tryCount--;
    }

    //wait 10 secs to ensure the sim was finished
    QThread::sleep(10);
    result = OPGUICore::api_is_simulation_running(this->response); 
    ASSERT_TRUE(result) << "Test: isSimulation running failed";
    ASSERT_EQ(this->response.isResponse(), false) << "Test: isRunning was expected to return false";
}


TEST_F(IS_SIMULATION_RUNNING_TEST, is_simulation_running_long_simulation_positive) {
    // Create XML config file
    this->tryCount=3;
    configXmlSimulationLong.replace("$$CORE_DIR$$", this->openPassCoreDir);
    bool fileCreated = TestHelpers::createAndCheckFile(this->configFileFullPath,configXmlSimulationLong);
    ASSERT_TRUE(fileCreated) << "Test: test sim config file could not be created";

    bool result = OPGUICore::api_run_opSimulationManager(this->responseRunSim); 

    // Check that the response is ok
    ASSERT_TRUE(result) << "Test: runOpSimulationManager returned failed";
    ASSERT_EQ(this->responseRunSim.getResponse(),this->response200Expected.getResponse()) << "Test: run op simulation function did not return expected 200 response";

    //Wait 1 sec before first try
    QThread::sleep(2);

    while(this->tryCount>0) {
        QThread::sleep(1);
        result = OPGUICore::api_is_simulation_running(this->response); 
        ASSERT_TRUE(result) << "Test: isSimulation running failed";
        //expect is running true
        ASSERT_EQ(this->response.isResponse(),true) << "Test: isRunning was expected to return true";
        this->tryCount--;
    }

    //wait 20 secs to ensure the sim was finished
     QThread::sleep(20);
    result = OPGUICore::api_is_simulation_running(this->response); 
    ASSERT_TRUE(result) << "Test: isSimulation running failed";
    ASSERT_EQ(this->response.isResponse(), false) << "Test: isRunning was expected to return false";
}


TEST_F(IS_SIMULATION_RUNNING_TEST, is_simulation_no_simulation_running_negative) {
    //create dummy config file as will not be used
    configXmlSimulationShort.replace("$$CORE_DIR$$", this->openPassCoreDir);
    bool fileCreated = TestHelpers::createAndCheckFile(this->configFileFullPath,configXmlSimulationShort);
    ASSERT_TRUE(fileCreated) << "Test: test sim config file could not be created";

    //Wait 1 sec before first try
    QThread::sleep(2);
    bool result = OPGUICore::api_is_simulation_running(this->response); 
    ASSERT_TRUE(result) << "Test: isSimulation running failed";
    ASSERT_EQ(this->response.isResponse(), false) << "Test: isRunning was expected to return false";
}







