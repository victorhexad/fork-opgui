/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include <QDir>
#include <QThread>
#include <OPGUICore.h>
#include <OAIPathRequest.h>
#include "OAIDefault200Response.h"
#include "test_helpers.h"
#include <QRegularExpression>

void checkLogValues(const QString& logContent, 
                    const QString& expectedLogLevel, 
                    const QString& expectedLogFile, 
                    const QString& expectedLibrary, 
                    const QString& expectedNumSimulations,
                    const QString& expectedSimulationExec) {

    ASSERT_FALSE(logContent.isEmpty()) << "Test: generated log file has not contents";

    QRegularExpression logLevelRegex("log level: (\\d+)");
    QRegularExpressionMatch logLevelMatch = logLevelRegex.match(logContent);
    ASSERT_EQ(logLevelMatch.captured(1), expectedLogLevel) << "Test: captured:"+logLevelMatch.captured(1).toStdString()+" expected:"+expectedLogLevel.toStdString();

    QRegularExpression logFileRegex("log file opSimulationManager: ([^\\n]+)");
    QRegularExpressionMatch logFileMatch = logFileRegex.match(logContent);
    ASSERT_EQ(logFileMatch.captured(1), expectedLogFile) << "Test: captured:"+logFileMatch.captured(1).toStdString()+" expected:"+expectedLogFile.toStdString();

    QRegularExpression librariesRegex("libraries: ([^\\n]+)");
    QRegularExpressionMatch librariesMatch = librariesRegex.match(logContent);
    ASSERT_EQ(librariesMatch.captured(1), expectedLibrary) << "Test: captured:"+librariesMatch.captured(1).toStdString()+" expected:"+expectedLibrary.toStdString();

    QRegularExpression numSimulationsRegex("number of simulations: (\\d+)");
    QRegularExpressionMatch numSimulationsMatch = numSimulationsRegex.match(logContent);
    ASSERT_EQ(numSimulationsMatch.captured(1), expectedNumSimulations) << "Test: captured:"+numSimulationsMatch.captured(1).toStdString()+" expected:"+expectedNumSimulations.toStdString();

    QRegularExpression simulationRegex("simulation: ([^\\n]+)");
    QRegularExpressionMatch simulationMatch = simulationRegex.match(logContent);
    ASSERT_EQ(simulationMatch.captured(1), expectedSimulationExec) << "Test: captured:"+simulationMatch.captured(1).toStdString()+" expected:"+expectedSimulationExec.toStdString();

}

QString configXml1Simulation = QString::fromStdString(
        R"(<?xml version="1.0" encoding="UTF-8"?>
        <opSimulationManager>
            <logLevel>5</logLevel>
            <simulation>opSimulation</simulation>
            <logFileSimulationManager>$$CORE_DIR$$opSimulationManager.log</logFileSimulationManager>
            <libraries>modules_TEST_TEMP</libraries>
            <simulationConfigs>
                <simulationConfig>
                    <configurations>$$CORE_DIR$$configs1</configurations>
                    <logFileSimulation>$$CORE_DIR$$log_TEST_TEMP_1.log</logFileSimulation>
                    <results>$$CORE_DIR$$results_TEST_TEMP_1</results>
                </simulationConfig>
            </simulationConfigs>
        </opSimulationManager>)"
);

QString configXml2Simulation = QString::fromStdString(
        R"(<?xml version="1.0" encoding="UTF-8"?>
        <opSimulationManager>
            <logLevel>5</logLevel>
            <simulation>opSimulation</simulation>
            <logFileSimulationManager>$$CORE_DIR$$opSimulationManager.log</logFileSimulationManager>
            <libraries>modules_TEST_TEMP</libraries>
            <simulationConfigs>
                <simulationConfig>
                    <configurations>$$CORE_DIR$$configs1</configurations>
                    <logFileSimulation>$$CORE_DIR$$log_TEST_TEMP_1.log</logFileSimulation>
                    <results>$$CORE_DIR$$results_TEST_TEMP_1</results>
                </simulationConfig>
                <simulationConfig>
                    <configurations>$$CORE_DIR$$configs2</configurations>
                    <logFileSimulation>$$CORE_DIR$$log_TEST_TEMP_2.log</logFileSimulation>
                    <results>$$CORE_DIR$$results_TEST_TEMP_2</results>
                </simulationConfig>
            </simulationConfigs>
        </opSimulationManager>)"
);
QString configXml3Simulation = QString::fromStdString(
        R"(<?xml version="1.0" encoding="UTF-8"?>
        <opSimulationManager>
            <logLevel>5</logLevel>
            <simulation>opSimulation</simulation>
            <logFileSimulationManager>$$CORE_DIR$$opSimulationManager_TEST_TEMP.log</logFileSimulationManager>
            <libraries>modules_TEST_TEMP</libraries>
            <simulationConfigs>
                <simulationConfig>
                    <configurations>$$CORE_DIR$$configs</configurations>
                    <logFileSimulation>$$CORE_DIR$$log_TEST_TEMP_1.log</logFileSimulation>
                    <results>$$CORE_DIR$$results_TEST_TEMP</results>
                </simulationConfig>
            </simulationConfigs>
        </opSimulationManager>)"
);

QString configXmlSimulationWrong = QString::fromStdString(
        R"(<?xml version="1.0" encoding="UTF-8"?>
        <opSimulationnager>
        <logLevel>2</logLevel>
        <logFileSimulationMa leSimulationManager>
        <simulation> mulation</simulation>
        </opSimlationManager>)"
);

//search ERROR

class RUN_OP_SIMULATION_MANAGER_TEST : public ::testing::Test {
  public:
    BackendConfig &test_conf = BackendConfig::getInstance();
    QString openPassCoreDir;
    QString configFileFullPath;
    QString backupFilePath;
    QString opSimulationExe;

    OpenAPI::OAIDefault200Response response;
    OpenAPI::OAIDefault200Response response200Expected;
    
    void SetUp() override {
        response200Expected.setResponse("Simulation started correctly");
        // Prepare the file paths
        this->opSimulationExe = QString::fromStdString(test_conf.OPSIMULATION_EXE);
        this->openPassCoreDir = QString::fromStdString(test_conf.PATH_OPENPASS_CORE);
        this->openPassCoreDir = (openPassCoreDir.endsWith("/") ? this->openPassCoreDir : this->openPassCoreDir + "/");

        this->configFileFullPath =  TestHelpers::joinPaths(this->openPassCoreDir,QString::fromStdString(test_conf.OPSIMULATION_MANAGER_XML));
        this->backupFilePath = this->configFileFullPath + QString::fromStdString("_bk");

        //check core path exist
        QDir dir(this->openPassCoreDir);
        ASSERT_TRUE(dir.exists())<< "SetUp: PATH_OPENPASS_CORE not existing:"+this->openPassCoreDir.toStdString();

        // Check if the file already exists and if yes, rename it
        QFile file(this->configFileFullPath);
        bool fileExists = file.exists();
        if (fileExists) {
            bool fileRenamed = file.rename(this->backupFilePath);
            ASSERT_TRUE(fileRenamed) << "SetUp: Failed to rename config file:"+configFileFullPath.toStdString();
        }
    }
    
    void TearDown() override {
        
        QThread::sleep(2);
        // Delete the newly created file
        bool removed = QFile::remove(this->configFileFullPath);
        ASSERT_TRUE(removed) << "TearDown: Failed to remove the test config file file:" + this->configFileFullPath.toStdString();

        QDir dir(this->openPassCoreDir);
        foreach(QString file, dir.entryList(QDir::Files)) {
            QString filePath = dir.absoluteFilePath(file);
            if (filePath.contains("TEST_TEMP")) {
                removed = QFile::remove(filePath);
                ASSERT_TRUE(removed) << "TearDown: Failed to remove the test file:" + filePath.toStdString();
            }
        }

        foreach(QString directory, dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot)) {
            QString innerDir = dir.absoluteFilePath(directory);
            if (innerDir.contains("TEST_TEMP")) {
                removed = QDir(innerDir).removeRecursively();
                ASSERT_TRUE(removed) << "TearDown: Failed to recursively remove the folder:" + innerDir.toStdString();
            }
        }

        // Rename the backup file back to its original name
        QFile backupFile(this->backupFilePath);
        if (backupFile.exists()) {
            bool fileRenamed = backupFile.rename(this->configFileFullPath);
            ASSERT_TRUE(fileRenamed) << "SetUp: Failed to rename the backuped config file";
        }
        
    }
};

TEST_F(RUN_OP_SIMULATION_MANAGER_TEST, Run_Op_Simulation_Manager_1_Simulation_Positive) {
    // Create XML config file
    configXml1Simulation.replace("$$CORE_DIR$$", this->openPassCoreDir);
    bool fileCreated = TestHelpers::createAndCheckFile(this->configFileFullPath,configXml1Simulation);
    ASSERT_TRUE(fileCreated) << "Test: test sim config file could not be created";

    bool result = OPGUICore::api_run_opSimulationManager(this->response); 

    // Check that the response is ok
    ASSERT_TRUE(result) << "Test: runOpSimulationManager returned failed";
    ASSERT_EQ(this->response.getResponse(),this->response200Expected.getResponse()) << "Test: run op simulation function did not return expected 200 response";

    //check if the log file of the simulation manager exists
    QThread::sleep(1);
    QFile simLogFile(TestHelpers::joinPaths(this->openPassCoreDir,"opSimulationManager.log"));
    ASSERT_TRUE(simLogFile.exists()) << "Test: Sim simulation manager log file was not created:" + simLogFile.fileName().toStdString();

    QDomElement root = TestHelpers::parseXMLString(configXml1Simulation);
    QDomElement simulationConfigs = root.firstChildElement("simulationConfigs");
    QDomElement simulationConfig = simulationConfigs.firstChildElement("simulationConfig");
    
    int countSimulations = 0;

    while(!simulationConfig.isNull()) {
        QString resultsDir = simulationConfig.firstChildElement("results").text();

        //check if results folder for the specific sim exists
        QThread::sleep(1);
        QDir dir(resultsDir);
        ASSERT_TRUE(dir.exists()) << "Test: No results folder created:" + resultsDir.toStdString();

        countSimulations++;

        // Move to the next simulationConfig element
        simulationConfig = simulationConfig.nextSiblingElement("simulationConfig");
    }
    QThread::sleep(1);
    QString logContents=TestHelpers::readFile(TestHelpers::joinPaths(this->openPassCoreDir,"opSimulationManager.log"));

    //check if log file contains errors

    checkLogValues(logContents
                    ,"5"
                    ,TestHelpers::joinPaths(this->openPassCoreDir,"opSimulationManager.log")
                    ,"modules_TEST_TEMP"
                    ,QString::number(countSimulations)
                    ,this->opSimulationExe);
}

TEST_F(RUN_OP_SIMULATION_MANAGER_TEST, Run_Op_Simulation_Manager_2_Simulations_Positive) {
    // Create XML config file
    configXml2Simulation.replace("$$CORE_DIR$$", this->openPassCoreDir);
    bool fileCreated = TestHelpers::createAndCheckFile(this->configFileFullPath,configXml2Simulation);
    ASSERT_TRUE(fileCreated) << "Test: test sim config file could not be created";

    bool result = OPGUICore::api_run_opSimulationManager(this->response);

    // Check that the response is ok
    ASSERT_TRUE(result) << "Test: runOpSimulationManager returned failed";
    ASSERT_EQ(this->response.getResponse(),this->response200Expected.getResponse()) << "Test: run op simulation function did not return expected 200 response";

    
    //check if the log file of the simulation manager exists
    QThread::sleep(1);
    QFile simLogFile(TestHelpers::joinPaths(this->openPassCoreDir,"opSimulationManager.log"));
    ASSERT_TRUE(simLogFile.exists()) << "Test: Sim simulation manager log file was not created:" + simLogFile.fileName().toStdString();

    QDomElement root = TestHelpers::parseXMLString(configXml2Simulation);
    QDomElement simulationConfigs = root.firstChildElement("simulationConfigs");
    QDomElement simulationConfig = simulationConfigs.firstChildElement("simulationConfig");
    
    int countSimulations = 0;

    while(!simulationConfig.isNull()) {
        QString resultsDir = simulationConfig.firstChildElement("results").text();

        //check if results folder for the specific sim exists
        QThread::sleep(1);
        QDir dir(resultsDir);
        ASSERT_TRUE(dir.exists()) << "Test: No results folder created:" + resultsDir.toStdString();

        countSimulations++;

        // Move to the next simulationConfig element
        simulationConfig = simulationConfig.nextSiblingElement("simulationConfig");
    }
    QThread::sleep(1);
    QString logContents=TestHelpers::readFile(TestHelpers::joinPaths(this->openPassCoreDir,"opSimulationManager.log"));

    //check if log file contains errors

    checkLogValues(logContents
                    ,"5"
                    ,TestHelpers::joinPaths(this->openPassCoreDir,"opSimulationManager.log")
                    ,"modules_TEST_TEMP"
                    ,QString::number(countSimulations)
                    ,this->opSimulationExe);
}


TEST_F(RUN_OP_SIMULATION_MANAGER_TEST, Run_Op_Simulation_Manager_Negative_No_exe) {
    QString managerExe = QString::fromStdString(this->test_conf.OPSIMULATION_MANAGER_EXE);
    configXmlSimulationWrong.replace("$$CORE_DIR$$", this->openPassCoreDir);
    bool fileCreated = TestHelpers::createAndCheckFile(this->configFileFullPath,configXmlSimulationWrong);
    ASSERT_TRUE(fileCreated) << "Test: test sim config file could not be created";

    QFile execFile(TestHelpers::joinPaths(this->openPassCoreDir,managerExe));
    ASSERT_TRUE(execFile.exists()) << "Test: Exec file does not exist";

    QString newPath = TestHelpers::joinPaths(this->openPassCoreDir,managerExe+"_bk");
    bool renamed = execFile.rename(newPath);
    ASSERT_TRUE(renamed) << "Test: Failed to rename the exe file file";

    bool result = OPGUICore::api_run_opSimulationManager(this->response); 

    // Expect that process can not be started as the exec is not found
    EXPECT_FALSE(result) << "Test: runOpSimulationManager returned ok but should return fail";

    renamed = execFile.rename(TestHelpers::joinPaths(this->openPassCoreDir,managerExe));
    ASSERT_TRUE(renamed) << "Test: Failed to rename the backed up exe file to original name";
}


/*
This test can be used if sim manager gives an error with wrong xml config
TEST_F(RUN_OP_SIMULATION_MANAGER_TEST, Run_Op_Simulation_Manager_Negative) {
    configXmlSimulationWrong.replace("$$CORE_DIR$$", this->openPassCoreDir);
    bool fileCreated = TestHelpers::createAndCheckFile(this->configFileFullPath,configXmlSimulationWrong);
    ASSERT_TRUE(fileCreated) << "Test: test sim config file could not be created";

    bool result = OPGUICore::api_run_opSimulationManager(this->response); 
    //this->response.setResponse("opsimulation manager data exported correctly!");
    //bool result = true;

    // Check that the XML file has been created
    ASSERT_FALSE(result) << "Test: runOpSimulationManager returned fail";
}
*/