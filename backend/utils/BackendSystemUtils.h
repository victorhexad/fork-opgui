/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#ifndef BACKEND_SYSTEMUTILS_H
#define BACKEND_SYSTEMUTILS_H

#include <string>

bool executeSystemCommand(const std::string& command, std::string& output);

#endif // BACKEND_SYSTEMUTILS_H