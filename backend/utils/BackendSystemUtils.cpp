/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <string>
#include <cstdio>
#include <cstring>
#include <iostream>

bool executeSystemCommand(const std::string& command, std::string& output)
{
    FILE* pipe = popen(command.c_str(), "r");
    if (pipe)
    {
        char buffer[128];
        while (!feof(pipe))
        {
            if (fgets(buffer, 128, pipe) != nullptr)
                output += buffer;
        }

        int status = pclose(pipe);
        if (status == 0)
        {
            return true; // Success
        }
        else
        {
            char errorMsg[128];
            std::snprintf(errorMsg, sizeof(errorMsg), "System command failed with exit code %d", status);
            output += errorMsg;
            std::cerr << errorMsg << std::endl;
        }
    }
    else
    {
        std::cerr << "Failed to execute the system command." << std::endl;
    }

    return false; // Error
}