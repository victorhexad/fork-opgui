/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <FileUtils.h>

namespace FileUtils
{

    auto 
    PathExists(std::string path)->bool
    {
       return std::filesystem::exists(path);
    }

    auto 
    FileExists(std::string file_path)->bool
    {
        return std::filesystem::exists(file_path);
    }

    auto 
    FolderExists(std::string folder_path)->bool
    {
        return std::filesystem::is_directory(folder_path);
    }

    auto 
    FolderExistsInsideWorkspace(std::string folder_path,std::string wksp_path)->bool
    {
        auto wksp_exists = FolderExists(wksp_path);
        int count = 0;

        if(folder_path.find(wksp_path) != std::string::npos)
        {
            return true;
        }
        else
        {
            return false;
        }

    }
    
    auto 
    IsFolder(std::string folder_path)->bool
    {
        return std::filesystem::is_directory(folder_path);
    }
    
    auto 
    IsFile(std::string file_path)->bool
    {
        return std::filesystem::is_regular_file(file_path);
    }

    auto 
    AllFilesInDir(std::string directory)->std::vector<FileUtilStruct>
    {

        std::vector<FileUtilStruct> vFiles;
        
        if(!FolderExists(directory))
        {
            return vFiles;
        }    
        
        for (const auto & entry : std::filesystem::directory_iterator(directory))
        {
            FileUtilStruct file;
            file.file_name = entry.path().filename().generic_string();
            file.file_path = entry.path().generic_string();
            vFiles.push_back(file);
        }
                
        return vFiles;
    }

    auto
    DeleteFilesInDir(std::string directory)->bool
    {
        for (const auto& entry : std::filesystem::directory_iterator(directory))
        {
            std::filesystem::remove_all(entry.path());       
        }

        auto files = FileUtils::AllFilesInDir(directory);

        if(files.size() > 0 )
        {
            return false;
        }
        
        if(FolderExists(directory) && (files.size() == 0))
        {
            return true;
        }

        
    }

    auto 
    CreateFolder(std::string directory)->bool
    {
       auto is_dir_created = std::filesystem::create_directories(directory);
       auto dir_exists = FolderExists(directory);

       if(is_dir_created && dir_exists)
       {
        return true;
       }
       else
       {
        return false;
       }
    }

}