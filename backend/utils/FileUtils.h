/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#ifndef FILE_UTILS_H
#define FILE_UTILS_H

#include <OPGUICoreIncludes.h>

namespace FileUtils
{
    struct FileUtilStruct
    {
        std::string file_name;
        std::string file_path;
    };

    auto PathExists(std::string path)->bool;
    auto FileExists(std::string file_path)->bool;
    auto FolderExists(std::string folder_path)->bool;
    auto FolderExistsInsideWorkspace(std::string folder_path,std::string wksp_path)->bool;
    auto IsFolder(std::string folder_path)->bool;
    auto IsFile(std::string file_path)->bool;
    auto AllFilesInDir(std::string directory)->std::vector<FileUtilStruct>;
    auto DeleteFilesInDir(std::string directory)->bool;
    auto CreateFolder(std::string directory)->bool;
    
}

#endif