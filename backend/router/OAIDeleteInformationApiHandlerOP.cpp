/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include "OAIDeleteInformationApiHandlerOP.h"
#include "OAIDefault200Response.h"
#include "OAIError404.h"
#include "OAIError500.h"
#include "OAIPathRequest.h"
#include "OAICustomHelpers.h"
#include "OPGUICore.h"


namespace OpenAPI {

    OAIDeleteInformationApiHandlerOP::OAIDeleteInformationApiHandlerOP() {

    }

    OAIDeleteInformationApiHandlerOP::~OAIDeleteInformationApiHandlerOP() {

    }

    void OAIDeleteInformationApiHandlerOP::deleteInformation(OAIPathRequest oai_path_request) 
    {
        //Q_UNUSED(oai_path_request);
        
        qDebug()<< "own impl delete called";
        auto reqObj = qobject_cast<OAIDeleteInformationApiRequest*>(sender());
        int code = 0;

        

        if( reqObj != nullptr )
        {
            OAIDefault200Response res;

            auto result = OPGUICore::api_delete_information(oai_path_request,res);
            

            if(result)
            {
                reqObj->deleteInformationResponse(res);
                return;
            }
            //return this for 404
            else if(!result)
            {
                code=404;
                OAIError404 error;
                error.setMessage("");
                error.setCode(code);
                handleSocketResponse(reqObj, error.asJsonObject(), code);

            }
            //return this for 500
            else
            {
                code=500;
                OAIError500 error;
                error.setMessage("Server error");
                error.setCode(code);
                handleSocketResponse(reqObj, error.asJsonObject(), code);
            } 
        }
    }
}
