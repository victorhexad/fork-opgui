/**
 * opGUI - OpenAPI 3.0
 * Dummy description about the available endpoints Some useful links: - [The opGUI repository](https://gitlab.eclipse.org/eclipse/openpass/opgui) - [The documentation page](https://www.eclipse.org/openpass/content/html/index.html)
 *
 * The version of the OpenAPI document: 3.0.1
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>
#include <QDebug>
#include <QNetworkReply>

#include "OAIExportOpsimulationManagerXmlApiHandlerOP.h"
#include "OAIExportOpsimulationManagerXmlApiRequest.h"

#include "OAIDefault200Response.h"
#include "OPGUICore.h"

#include "OPGUILogger.h"


namespace OpenAPI {

    OAIExportOpsimulationManagerXmlApiHandlerOP::OAIExportOpsimulationManagerXmlApiHandlerOP() {

        //qDebug() << "[INFO] Constructor called";

    }

    OAIExportOpsimulationManagerXmlApiHandlerOP::~OAIExportOpsimulationManagerXmlApiHandlerOP() {

    }

    void OAIExportOpsimulationManagerXmlApiHandlerOP::apiExportOpsimulationManagerXmlPost(OAIOpSimulationManagerXmlRequest oaiop_simulation_manager_xml_request) {
        auto reqObj = qobject_cast<OAIExportOpsimulationManagerXmlApiRequest*>(sender());
        logging::DEBUG("reached request");
        
        if( reqObj != nullptr )
        {
            logging::DEBUG("reached request");
            OAIDefault200Response res;
            OPGUICore::api_export_opSimulationManager_xml(oaiop_simulation_manager_xml_request,res);
            reqObj->apiExportOpsimulationManagerXmlPostResponse(res);
        }
    }

}

