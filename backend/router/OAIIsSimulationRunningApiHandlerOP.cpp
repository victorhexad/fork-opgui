/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>
#include <QDebug>

#include "OAIIsSimulationRunningApiHandlerOP.h"
#include "OAIIsSimulationRunningApiRequest.h"
#include "OAICustomHelpers.h"

#include "OPGUICore.h"

namespace OpenAPI {

OAIIsSimulationRunningApiHandlerOP::OAIIsSimulationRunningApiHandlerOP(){

}

OAIIsSimulationRunningApiHandlerOP::~OAIIsSimulationRunningApiHandlerOP(){

}

void OAIIsSimulationRunningApiHandlerOP::apiIsSimulationRunningGet() {
    auto reqObj = qobject_cast<OAIIsSimulationRunningApiRequest*>(sender());
    //RISHAB TODO CALL HERE THE OPGUI CORE FUNCTION AND STUFF 
    //RISHAB ---> if simulation is running set res->setResponse(true)
    //RISHAB ---> if simulation is running set res->setResponse(false)
    //if there is some error return error with this handleSocketResponse.... (like you do with the other endpoints)

    if( reqObj != nullptr )
    {
        OAIBoolean200Response res;
        
        bool success = OPGUICore::api_is_simulation_running(res);
            
        if(success)
        {
            reqObj->apiIsSimulationRunningGetResponse(res);
        }
        else
        {
            qDebug() << "System command execution failed with error: Internal Error";
            handleSocketResponse(reqObj, "", 500);
        }

    }
}


}
