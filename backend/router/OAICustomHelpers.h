/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <QNetworkReply>
#include "HttpUtils.h"

template<typename T>
inline static void handleSocketResponse(T& object, std::string message, int code) {
    QString serverError("something went wrong with the server, please contact your administrator.");
    QString clientError("something went wrong with the client, please contact your administrator.");
    QString successful("operation successful");
    QString messageStr("");

    code = (code>0 && code<1000)?code:999; //unknown

    QHttpEngine::Socket* socket = object->getRawSocket();
    socket->setStatusCode(code);

    if(HttpStatus::isSuccessful(code)) {
        messageStr = (message.empty()) ? successful : QString::fromStdString(message);
    }else if(HttpStatus::isServerError(code)) {
        messageStr = (message.empty()) ? serverError : QString::fromStdString(message);
    }else if(HttpStatus::isClientError(code)) {
        messageStr = (message.empty()) ? clientError : QString::fromStdString(message);
    }else {
        messageStr = (message.empty()) ? HttpStatus::reasonPhrase(code) : QString::fromStdString(message);
    }

    QByteArray messageB = messageStr.toUtf8();

    //QUnused(networkError)
    QNetworkReply::NetworkError networkErrorCode = HttpStatus::statusCodeToNetworkError(code);
    object->sendCustomResponse(messageB, networkErrorCode);
}

template<typename T>
inline static void handleSocketResponse(T& object, QJsonObject messageObj, int code) {
    QString serverError("something went wrong with the server, please contact your administrator.");
    QString clientError("something went wrong with the client, please contact your administrator.");
    QString successful("operation successful");
    QString messageStr("");

    code = (code>0 && code<1000)?code:999; //unknown

    QHttpEngine::Socket* socket = object->getRawSocket();
    socket->setStatusCode(code);

    if(HttpStatus::isSuccessful(code)) {
        messageStr = successful;
    }else if(HttpStatus::isServerError(code)) {
        messageStr = serverError;
    }else if(HttpStatus::isClientError(code)) {
        messageStr = clientError;
    }else {
        messageStr = HttpStatus::reasonPhrase(code);
    }

    if(messageObj.find("message") != messageObj.end() && messageObj["message"].toString().isEmpty()){
        messageObj.insert("message", messageStr);
    }

    QJsonDocument messageDoc(messageObj);
    QByteArray messageB = messageDoc.toJson();

    //QUnused(networkError)
    QNetworkReply::NetworkError networkErrorCode = HttpStatus::statusCodeToNetworkError(code);
    object->sendCustomResponse(messageB, networkErrorCode);
}
