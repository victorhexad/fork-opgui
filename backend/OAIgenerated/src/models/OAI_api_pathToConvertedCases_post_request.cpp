/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include "OAI_api_pathToConvertedCases_post_request.h"

#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QObject>

#include "OAIHelpers.h"

namespace OpenAPI {

OAI_api_pathToConvertedCases_post_request::OAI_api_pathToConvertedCases_post_request(QString json) {
    this->initializeModel();
    this->fromJson(json);
}

OAI_api_pathToConvertedCases_post_request::OAI_api_pathToConvertedCases_post_request() {
    this->initializeModel();
}

OAI_api_pathToConvertedCases_post_request::~OAI_api_pathToConvertedCases_post_request() {}

void OAI_api_pathToConvertedCases_post_request::initializeModel() {

    m_path_isSet = false;
    m_path_isValid = false;
}

void OAI_api_pathToConvertedCases_post_request::fromJson(QString jsonString) {
    QByteArray array(jsonString.toStdString().c_str());
    QJsonDocument doc = QJsonDocument::fromJson(array);
    QJsonObject jsonObject = doc.object();
    this->fromJsonObject(jsonObject);
}

void OAI_api_pathToConvertedCases_post_request::fromJsonObject(QJsonObject json) {

    m_path_isValid = ::OpenAPI::fromJsonValue(path, json[QString("path")]);
    m_path_isSet = !json[QString("path")].isNull() && m_path_isValid;
}

QString OAI_api_pathToConvertedCases_post_request::asJson() const {
    QJsonObject obj = this->asJsonObject();
    QJsonDocument doc(obj);
    QByteArray bytes = doc.toJson();
    return QString(bytes);
}

QJsonObject OAI_api_pathToConvertedCases_post_request::asJsonObject() const {
    QJsonObject obj;
    if (m_path_isSet) {
        obj.insert(QString("path"), ::OpenAPI::toJsonValue(path));
    }
    return obj;
}

QString OAI_api_pathToConvertedCases_post_request::getPath() const {
    return path;
}
void OAI_api_pathToConvertedCases_post_request::setPath(const QString &path) {
    this->path = path;
    this->m_path_isSet = true;
}

bool OAI_api_pathToConvertedCases_post_request::is_path_Set() const{
    return m_path_isSet;
}

bool OAI_api_pathToConvertedCases_post_request::is_path_Valid() const{
    return m_path_isValid;
}

bool OAI_api_pathToConvertedCases_post_request::isSet() const {
    bool isObjectUpdated = false;
    do {
        if (m_path_isSet) {
            isObjectUpdated = true;
            break;
        }
    } while (false);
    return isObjectUpdated;
}

bool OAI_api_pathToConvertedCases_post_request::isValid() const {
    // only required properties are required for the object to be considered valid
    return true;
}

} // namespace OpenAPI
