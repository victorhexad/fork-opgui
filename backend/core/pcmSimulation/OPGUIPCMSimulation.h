/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#pragma once

#ifndef OPGUI_PCM_SIMULATION_H
#define OPGUI_PCM_SIMULATION_H


#include "ConfigurationGeneratorPcm/ConfigGeneratorPcm.h"
#include "FileHelper.h"
#include "GUI_Definitions.h"

#include <string>
#include <vector>

#include <QString>
#include <QStringList>

#include <OPGUILogger.h> 

namespace OPGUIPCMSimulation
{

struct PCMConfigResult 
{
    std::string path_config;
    std::string path_simulation_manager_config;
};

struct PCMSimulationResult 
{
    std::string path_result;;
};

class PCMSimulation
{
    public:

    std::string pcmDBPath;
    QStringList agentsCar1; 
    QStringList agentsCar2;
    QStringList agentsCarOther;
    std::string pathModulesFolder;
    std::string pathOpSimulation;
    std::string pathPCMResultFolder;
    std::string logLevel;
    
    QStringList pcmCases;
    QStringList pcmSelectedCasesForSimulation;

    PCMSimulation() = default;
    
    PCMSimulation(std::string pcmDBPath,
    QStringList agentsCar1, 
    QStringList agentsCar2,
    QStringList agentsCarOther,
    std::string pathModulesFolder,
    std::string pathPCMResultFolder,
    std::string pathOpSimulation,
    std::string pathOpSimulationManager);


    //    std::string pathPCMResultFolder,
    //      std::string loglevel
    
    ~PCMSimulation();

    /* ENDPOINT FUNCTIONS */
    bool SetPathToConvertedCases(std::string path);
    std::vector<std::string> UIGetListOfPCMCases();
    PCMConfigResult UIGenerateConfigOfPCMCases(QStringList pcmSelectedCasesForSimulation);
    PCMSimulationResult UIStartSimulation();
    bool GenerateSimulationXml(QList<OpenAPI::OAISimulationConfig> simConfig,int logLevel,QString logFile,QString libraries,QString simulation);
    bool LoadCasesFromPcmFile(QString path,QStringList &pcm_cases);

    

    private:

    QString pathGeneratedOPSimulationManagerConfig;
    bool inputFromPCMDB = true;
    int variationCount = VARIATION_COUNT_DEFAULT;
    int initRandomSeed = INIT_RANDOM_SEED;

    bool LoadCasesFromPcmFile();

    bool GenerateConfigs();
    bool RunSimulation();


    QStringList GetListOfPCMCases();
    PCMConfigResult GenerateConfigOfPCMCases(QStringList pcmSelectedCasesForSimulation);
    bool CreateConfigs(ConfigGenerator &configGenerator, QStringList &pcmCaseIndexList, QStringList &otherSytemList, QStringList &car1SystemList, QStringList &car2SystemList);
    PCMSimulationResult StartSimulation();
    PCM_SimulationSet* ReadSimulationSet(bool inputFromDb, QString pcmCase);
    PCM_SimulationSet* ReadSimulationSetFromDb(QString pcmCase);
    void RelocateCog(PCM_SimulationSet *simulationSet);

};

}

#endif
