/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <OPGUIPCMSimulation.h>
#include <OPGUICoreIncludes.h>
#include <OPGUICoreConfig.h>

#include <exception>

namespace OPGUIPCMSimulation
{
    PCMSimulation::PCMSimulation(std::string pcmDBPath,
    QStringList agentsCar1, 
    QStringList agentsCar2,
    QStringList agentsCarOther,
    std::string pathModulesFolder,
    std::string pathPCMResultFolder,
    std::string pathOpSimulation,
    std::string pathOpSimulationManager
)
    {
        
        this->pcmDBPath = pcmDBPath;
        this->agentsCar1 = agentsCar1;
        this->agentsCar2 = agentsCar2;
        this->agentsCarOther = agentsCarOther;
        this->pathModulesFolder = pathModulesFolder;
        this->pathOpSimulation = pathOpSimulation;
        this->pathPCMResultFolder = pathPCMResultFolder;

        // Initialised Using APIS
        this->logLevel = "";
        this->pathGeneratedOPSimulationManagerConfig = "";
    }

    PCMSimulation::~PCMSimulation()
    {
        this->pcmDBPath = "";
        this->agentsCar1.clear();
        this->agentsCar2.clear();
        this->agentsCarOther.clear();
        this->pathModulesFolder = "";
        this->pathOpSimulation = "";
        this->pathPCMResultFolder = "";
        this->logLevel = "";
    
        pcmCases.clear();
        pcmSelectedCasesForSimulation.clear();

        this->pathGeneratedOPSimulationManagerConfig = "";
    }

    std::vector<std::string> PCMSimulation::UIGetListOfPCMCases()
    {
        std::vector<std::string> res_pcm_cases;
        QStringList pcm_cases = this->GetListOfPCMCases();
        for(auto pcm_case : pcm_cases)
        {
            res_pcm_cases.push_back(pcm_case.toStdString());
        }

        return res_pcm_cases;
    }
    
    PCMConfigResult PCMSimulation::UIGenerateConfigOfPCMCases(QStringList pcmSelectedCasesForSimulation)
    {
        return GenerateConfigOfPCMCases(pcmSelectedCasesForSimulation);
    }
    
    PCMSimulationResult PCMSimulation::UIStartSimulation()
    {
        return StartSimulation();
    }
    
    QStringList PCMSimulation::GetListOfPCMCases()
    {
        LoadCasesFromPcmFile();
        return this->pcmCases;    
    }

    PCMConfigResult PCMSimulation::GenerateConfigOfPCMCases(QStringList pcmSelectedCasesForSimulation)
    {
        
        PCMConfigResult configResult;
        
        // TODO:
        this->pcmSelectedCasesForSimulation = pcmSelectedCasesForSimulation;
        GenerateConfigs();

        configResult.path_config = this->pathPCMResultFolder;
        configResult.path_simulation_manager_config = this->pathGeneratedOPSimulationManagerConfig.toStdString();

        return configResult;
    }
    
    PCMSimulationResult PCMSimulation::StartSimulation()
    {
        PCMSimulationResult simResult;
        // TODO:
        RunSimulation();
        simResult.path_result = "";
        return simResult;
    }

    bool PCMSimulation::LoadCasesFromPcmFile()
    {
        logging::INFO("Loading PCM DB.");
    
        DatabaseReader dbReader;

        if(this->pcmDBPath.empty())
        {
            return false;
        }

        //QString db_path = "C:/Users/Rishabh Srivastava/Downloads/PCM_Format_v5.0_Testszenario.db";
        QString db_path = QString::fromStdString(this->pcmDBPath);

        logging::INFO("Reading DB located at : " + db_path.toStdString());

        dbReader.SetDatabase(db_path);
        bool success = dbReader.OpenDataBase();
    
        if (success)
        {
            logging::INFO("DB Reading Success.");
            success = dbReader.ReadCaseList(this->pcmCases);
        }

        if (!success)
        {
            logging::ERROR("DB Reading failed.");
            return false;
        }

        dbReader.CloseDataBase();

        return true;
    }
    
    bool PCMSimulation::LoadCasesFromPcmFile(QString path,QStringList &pcm_cases)
    {
        logging::INFO("Loading PCM DB.");
    
        DatabaseReader dbReader;

        if(path.isEmpty())
        {
            logging::ERROR("PCM DB File path is Empty / Invalid");
            return false;
        }

        //QString db_path = "C:/Users/Rishabh Srivastava/Downloads/PCM_Format_v5.0_Testszenario.db";
        //QString db_path = QString::fromStdString(this->pcmDBPath);

        logging::INFO("Reading DB located at : " + path.toStdString());

        dbReader.SetDatabase(path);
        bool success = dbReader.OpenDataBase();

        if (success)
        {
            logging::INFO("DB Reading Success.");
            success = dbReader.ReadCaseList(pcm_cases);
        }

        if (!success)
        {
            logging::ERROR("DB Reading failed.");
            return false;
        }

        dbReader.CloseDataBase();

        return true;
    }


    bool PCMSimulation::GenerateConfigs()
    {
        QStringList pcmCaseIndexList = this->pcmSelectedCasesForSimulation;  
        
        logging::INFO("CASES LOADED");

        QStringList otherSytemList = this->agentsCarOther;
        QStringList car1SystemList = this->agentsCar1;
        QStringList car2SystemList = this->agentsCar2;

        logging::INFO("AGENTS LOADED");

        // HARDCODED 
        int logLvl = 5; //std::atoi(this->logLevel);
        logging::INFO("LOG LEVEL SET");

        ConfigGenerator configGenerator(QString::fromStdString(this->pathPCMResultFolder));
        //ConfigGenerator configGenerator(QString::fromStdString(BackendConfig::getInstance().PATH_OPENPASS_CORE));
        
        logging::INFO("Config Generator Initialised");

        CreateConfigs(configGenerator,
                  pcmCaseIndexList,
                  otherSytemList,
                  car1SystemList,
                  car2SystemList);
        
        logging::INFO("CreateConfigsDirect Executed");
        logging::INFO("opSimulationManager.xml Construction started.");
    
        this->pathGeneratedOPSimulationManagerConfig = configGenerator.GenerateFrameworkConfig(logLvl);
        
        if (this->pathGeneratedOPSimulationManagerConfig.isEmpty())
        {
            logging::ERROR("opSimulationManager.xml Construction failed.");
            return false;
        }

        return true;

    }

    bool PCMSimulation::CreateConfigs(ConfigGenerator &configGenerator, 
                                        QStringList &pcmCaseIndexList, 
                                        QStringList &otherSytemList, 
                                        QStringList &car1SystemList, 
                                        QStringList &car2SystemList)
    {
        for (auto &pcmCaseIndex : this->pcmSelectedCasesForSimulation)
        {

            // Prepare to read a simulation set
            QString pcmCase = QString("%1").arg(pcmCaseIndex.toInt());
            PCM_SimulationSet *simulationSet = ReadSimulationSet(inputFromPCMDB, pcmCase);
            if (simulationSet == nullptr)
            {
                logging::ERROR("Failed to read data for case: " + pcmCase.toStdString());
                return false;
            }

            int otherSystemCount = 0;
            for (QString otherSystem : otherSytemList)
            {
                if (otherSystem.isEmpty())
                {
                    continue;
                }
                int car1SystemCount = 0;
                for (QString car1System : car1SystemList)
                {
                    if (car1System.isEmpty())
                    {
                        continue;
                    }
                    int car2SystemCount = 0;
                    for (QString car2System : car2SystemList)
                    {
                        if (car2System.isEmpty())
                        {
                            continue;
                        }

                        QString systemName = QString::number(otherSystemCount) + "-" + QString::number(car1SystemCount) + "-" + QString::number(car2SystemCount);
                        QString caseSystemName = pcmCase + "/" + systemName;

                        for (int varIndex = 0; varIndex <= variationCount; varIndex++)
                        {
                            QString varName = "";
                            bool withVariation = false;

                            if (varIndex == 0)
                            {
                                varName = DIR_NO_VARIATION;
                                withVariation = false;
                            }
                            else
                            {
                                varName = QString("Var_%1").arg(varIndex, 5, 10, QChar('0')); // zero padding if var index less than 5 digits
                                withVariation = true;
                            }

                            QString caseSystemVarName = caseSystemName; // + "/" + varName; -> Remove Default
                            //QString caseSystemVarName = caseSystemName + "/" + varName; 

                            // the random seed uses PCM case number if the inital seed is negative. Otherwise it uses the inital seed.
                            int randomSeed = (initRandomSeed < 0) ? (pcmCaseIndex.toInt() + varIndex)
                                                              : (initRandomSeed + varIndex);

                            //if (withVariation)
                            //{
                            //    SaveState(simulationSet);
                            //    ApplyVariation(simulationSet, randomSeed);
                            //}

                            if (!configGenerator.GenerateConfigs(pcmCase,
                                                             caseSystemVarName,
                                                             otherSystem, car1System, car2System,
                                                             randomSeed,
                                                             simulationSet))
                            {

                                logging::ERROR("Failed to generate configuration file for case: " + pcmCase.toStdString());
      
                                if (simulationSet != nullptr)
                                {
                                    delete simulationSet;
                                }

                                return false;
                            }

                            //if (withVariation)
                            //{
                            //   ResetState(simulationSet);
                            //}
                        }

                        car2SystemCount++;
                    }
                    car1SystemCount++;
                }
                otherSystemCount++;
            }

            if (simulationSet != nullptr)
            {
                delete simulationSet;
            }
        }
        
        return true;
    }

    PCM_SimulationSet *PCMSimulation::ReadSimulationSet(bool inputFromDb, QString pcmCase)
    {
        //if (inputFromDb)
        //{
            return ReadSimulationSetFromDb(pcmCase);
        //}
        //else
        //{
        //    return ReadSimulationSetFromPrevCase(pcmCase);
        //}
    }

    PCM_SimulationSet *PCMSimulation::ReadSimulationSetFromDb(QString pcmCase)
    {
        // Read from database
        QString db_path = QString::fromStdString(this->pcmDBPath);

        DatabaseReader dbReader; 
        dbReader.SetDatabase(db_path);
        PCM_SimulationSet *simulationSet = dbReader.Read(pcmCase);

        if (simulationSet == nullptr)
        {
            logging::ERROR("Failed to read database for case: " + pcmCase.toStdString());
            return nullptr;
        }

        // Relocate trajectory because pcm cog is in the geometric middle of the car
        // but simulated cog is between the rear axle
        RelocateCog(simulationSet);

        return simulationSet;
    }

    void PCMSimulation::RelocateCog(PCM_SimulationSet *simulationSet)
    {
        for (uint i = 0; i < simulationSet->GetTrajectories().size(); i++)
        {
            if (simulationSet->GetParticipants().at(i)->GetType() == "0")
            {
                double wheelBase = simulationSet->GetParticipants().at(i)->GetWheelbase().toDouble();
                double distCgfa = simulationSet->GetParticipants().at(i)->GetDistcgfa().toDouble();
                double distanceRearAxleToCOG = wheelBase - distCgfa;

                simulationSet->GetTrajectories().at(i)->ShiftForward(distanceRearAxleToCOG);
            }
        }
    }


    bool PCMSimulation::RunSimulation()
    {
        logging::INFO("Starting Sim Direct");
    
        logging::INFO("Starting Simulation Manager From Config Generated at:" + this->pathGeneratedOPSimulationManagerConfig.toStdString());

        logging::INFO("Config Generated, Simulation Finished, Results Generated");

        return true;

    }

    bool PCMSimulation::GenerateSimulationXml(QList<OpenAPI::OAISimulationConfig> simConfig,int logLevel,QString logFile,QString libraries,QString simulation)
    {
        try
        {

            auto &config = BackendConfig::getInstance();

            if(QString::fromStdString(config.PATH_OPENPASS_CORE).isEmpty() || QString::fromStdString(config.PATH_OPENPASS_CORE) == "")
            {
                logging::ERROR("Error, PATH_OPENPASS_CORE path empty / invalid. Check config file.");
                return false;
            }

            ConfigGenerator configGenerator(QString::fromStdString(config.PATH_OPENPASS_CORE));
            logging::INFO("Config Generator Initialised");
            auto file_path = configGenerator.GenerateFrameworkConfigV2(QString::fromStdString(config.PATH_OPENPASS_CORE),logLevel,logFile,simulation,libraries,simConfig);

            if(file_path.isEmpty())
            {
                logging::ERROR("OPSIMULATIONMANAGER XML GENERATION FAILED");
                return false;            
            }
            else
            {
                logging::INFO("OPSIMULATION MANAGER GENERATED");
                return true;
            }
        }
        catch(QException exp)
        {
            logging::ERROR("Exception in api_export_opSimulationManager_xml : " + std::string(exp.what()));
            return false;
        }
        catch(std::exception std_exp)
        {
            logging::ERROR("Exception in api_export_opSimulationManager_xml : " + std::string(std_exp.what()));
            return false; 
        }

    }

    bool PCMSimulation::SetPathToConvertedCases(std::string path)
    {
        this->pathPCMResultFolder = path;
        return true;
    }

}    
