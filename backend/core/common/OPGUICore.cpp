/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <OPGUICore.h>
#include <OPGUICoreConfig.h>
#include <OPGUICoreConstants.h>
#include <OPGUIPCMSimulation.h>
#include <OPGUILogger.h>
#include <FileUtils.h>
#include <BackendSystemUtils.h>

#include <QProcess>
#include <QException>
#include <QtConcurrent>
#include <QStack>
#include <QDir>
#include <QFileInfoList>
#include <QFileInfo>


#include <exception>

namespace OPGUICore
{

    /*
        Sim Plugin Pointer
    */
    OPGUIPCMSimulation::PCMSimulation *sim = nullptr;
    
    /*
        POST
        /verify-path
        Verify if a path exists and has data
        
        Request :
        {
            "path": "/test/abc/deg"
        }

        Response :
        {
            "ok": true,
            "realPath": "C:/Users/$username/Documents/workspaces/opGUI/workspace1/test/abc/deg",
                         C:\Users\Rishabh Srivastava\Documents\workspaces\opGUI\workspace1\test
            "empty": false
        }       
    */

    bool api_verify_path(OpenAPI::OAIPathRequest req, OpenAPI::OAIVerifyPath_200_response &resp)
    {
        resp.setOk(false);
        resp.setEmpty(true);
        QString workspace_path = QString::fromStdString(BackendConfig::getInstance().WORKSPACE);

        try
        {
            logging::INFO("Initialising : api_verify_path");
            logging::INFO("workspace_path : " + workspace_path.toStdString());

            if(workspace_path.isEmpty())
            {
                logging::ERROR("Error, Workspace path empty / invalid. Check config file.");
                return false;
            }

            QString searched_path = req.getPath();
            logging::INFO("req_path : " + searched_path.toStdString());

            if(searched_path.isEmpty())
            {
                logging::ERROR("Internal Error, api_verify_path request path empty / invalid.");
                return false;
            }

            int count = 0;
            bool isFile = false;
            bool isDir = false;
            bool isEmptyDir = false;
            bool found = false;

            QDir dir(workspace_path);
            QFileInfoList entries = dir.entryInfoList(QDir::Files | QDir::Dirs | QDir::NoDotAndDotDot);

            QStack<QDir> dirsToCheck; // Using a stack to avoid recursion
            dirsToCheck.push(dir);
            while(!dirsToCheck.empty()){
                QDir currentDir = dirsToCheck.top();
                dirsToCheck.pop();

                QFileInfoList currentEntries = currentDir.entryInfoList(QDir::Files | QDir::Dirs | QDir::NoDotAndDotDot);
                for(const QFileInfo &entry : currentEntries) {
                    QString lastPart = entry.fileName();
                    if(QDir::cleanPath(lastPart).remove(QChar('/')) == QDir::cleanPath(searched_path).remove(QChar('/'))) {
                        count++;
                        if(count > 1) {
                            logging::INFO("Folder/File found more than once inside the workspace directory or subdirectories");
                            resp.setEmpty(true);
                            resp.setOk(false);
                            resp.setRealPath("Folder/File found more than once inside the workspace directory or subdirectories");
                            return true;
                            break;
                        }
                        logging::INFO("Found searched item in the workspace with absoulte path:"+entry.absoluteFilePath().toStdString());
                        isFile = entry.isFile();
                        isDir = entry.isDir();
                        isEmptyDir = isDir && QDir(entry.absoluteFilePath()).entryInfoList(QDir::NoDotAndDotDot | QDir::AllEntries).count() == 0;

                        logging::INFO(QString("Path is a %1").arg(isFile ? "file" : "folder").toStdString());
                        resp.setEmpty(isEmptyDir);
                        resp.setRealPath(entry.absoluteFilePath());
                        resp.setOk(true);
                        found = true;
                    }
                    if(entry.isDir()){
                        dirsToCheck.push(QDir(entry.absoluteFilePath()));
                    }
                }
            }
            if(!found){
                logging::INFO("Path was not found under the workspace:"+workspace_path.toStdString());
                resp.setEmpty(true);
                resp.setRealPath("Path was not found in workspace");
                resp.setOk(false);
            }
            return true;
        }
        catch(QException exp)
        {
            logging::ERROR("Exception in api_verify_path : " + std::string(exp.what()));
            return false;
        }
        catch(std::exception std_exp)
        {
            logging::ERROR("Exception in api_verify_path : " + std::string(std_exp.what()));
            return false; 
        }
        return false;
    }


    /*
        POST
        /delete-information
        Detele information of given folder
        
        Request :
        {
            "path": "/os/workspace/path/to/file"
        }

        Response :
        {
            "ok": true
        }        
    */
    bool api_delete_information(OpenAPI::OAIPathRequest req, OpenAPI::OAIDefault200Response &res)
    {
        try
        {

            logging::INFO("Initialising : api_delete_information");
            logging::INFO("Requested delete path : " + req.getPath().toStdString());

            if(req.getPath().toStdString().empty() || req.getPath().toStdString() == "")
            {
                logging::ERROR("Internal Error, api_delete_information request path empty / invalid.");
                return false;
            }

            auto path_exists = FileUtils::FolderExists(req.getPath().toStdString());

            if(!path_exists)
            {
                logging::ERROR("Invalid Path, Path does not exist : " + req.getPath().toStdString());
                res.setResponse("Invalid Path, Path does not exist");
                return false;
            }
            else if(path_exists)
            {
                auto is_deleted = FileUtils::DeleteFilesInDir(req.getPath().toStdString());
                
                if(!is_deleted)
                {
                    logging::ERROR("Error in deleting content. Check Permissions in path : " + req.getPath().toStdString());
                    res.setResponse("Error in deleting content. Check Permissions.");
                    return false;
                }

                if(is_deleted)
                {
                    logging::INFO("Files deleted successfully. in path : " + req.getPath().toStdString());
                    res.setResponse("true");
                    return true;
                } 
            }
        }
        catch(QException exp)
        {
            logging::ERROR("Exception in api_delete_information : " + std::string(exp.what()));
            return false;
        }
        catch(std::exception std_exp)
        {
            logging::ERROR("Exception in api_delete_information : " + std::string(std_exp.what()));
            return false; 
        }

    }

    /*
        GET
        /api/isSimulationRunning:
        execute opSimulationManager
        
        Request :
            NULL

        Response :
        {
            "key": "value"
        }      
    */
    bool api_is_simulation_running(OpenAPI::OAIBoolean200Response &resp)
    {
        try
        {

            logging::INFO("Initialising : api_is_Simulation_Finished");

            if(isRunning == true)
            {
                logging::INFO("Simulation is still running. Simulation has not finished.");
                resp.setResponse(true);
                return true;
            }
            else
            {
                logging::INFO("Simulation is not running.");
                resp.setResponse(false);
                return true;
            }
            
        }
        catch(QException exp)
        {
            logging::ERROR("Exception in api_is_simulation_running : " + std::string(exp.what()));
            return false;
        }
        catch(std::exception std_exp)
        {
            logging::ERROR("Exception in api_is_simulation_running : " + std::string(std_exp.what()));
            return false; 
        }

    }

    
    /*
        GET
        /runOpSimulationManager
        execute opSimulationManager
        
        Request :
            NULL

        Response :
        {
            "key": "value"
        }      
    */
    bool api_run_opSimulationManager(OpenAPI::OAIDefault200Response &resp)
    {   

        try
        {
            if(isRunning == false)
            {

            isRunning = true;
        
            logging::INFO("Initialising : api_run_opSimulationManager");
            
            if(QString::fromStdString(BackendConfig::getInstance().PATH_OPENPASS_CORE).isEmpty() || QString::fromStdString(BackendConfig::getInstance().PATH_OPENPASS_CORE) == "")
            {
                logging::ERROR("Error, PATH_OPENPASS_CORE path empty / invalid. Check config file.");
                isRunning = false;
                return false;
            }

            if(QString::fromStdString(BackendConfig::getInstance().OPSIMULATION_MANAGER_EXE).isEmpty() || (QString::fromStdString(BackendConfig::getInstance().OPSIMULATION_MANAGER_EXE) == ""))
            {
                logging::ERROR("Error, OPSIMULATION_MANAGER_EXE path empty / invalid. Check config file.");
                isRunning = false;
                return false;
            }

            if(QString::fromStdString(BackendConfig::getInstance().OPSIMULATION_MANAGER_XML).isEmpty() || QString::fromStdString(BackendConfig::getInstance().OPSIMULATION_MANAGER_XML) == "")
            {
                logging::ERROR("Error, OPSIMULATION_MANAGER_XML path empty / invalid. Check config file.");
                isRunning = false;
                return false;
            }



            std::string opSimulationExecutable = BackendConfig::getInstance().PATH_OPENPASS_CORE +"/"+ BackendConfig::getInstance().OPSIMULATION_MANAGER_EXE;
            auto opSimulationExecutableExists = FileUtils::FileExists(opSimulationExecutable);

            if(!opSimulationExecutableExists){
                logging::ERROR("Fatal Error During Initialisation, OpSimulation Executable  is not Found at : " + opSimulationExecutable);
                isRunning = false;
                return false;
            }

            QtConcurrent::run(
                []()
                {
                    QString path_core = QString::fromStdString(BackendConfig::getInstance().PATH_OPENPASS_CORE);
                    QString path_op_simulation_manager_exe = path_core + "/" + QString::fromStdString(BackendConfig::getInstance().OPSIMULATION_MANAGER_EXE);
                    logging::INFO("Starting Simulation Manager with opSimulationManager at : " + path_op_simulation_manager_exe.toStdString());

                    QString path_op_sim_xml = path_core + "/" + QString::fromStdString(BackendConfig::getInstance().OPSIMULATION_MANAGER_XML);
        
                    QStringList qArguments;
                    qArguments << "--config" << path_op_sim_xml;
                    logging::INFO("Starting Simulation Manager as : " + qArguments[0].toStdString() + " " + qArguments[1].toStdString());
        
                    QProcess* newProcess = new QProcess();
                    newProcess->start(path_op_simulation_manager_exe,qArguments);

                    if(newProcess->processId() == 0)
                    {
                        logging::ERROR("Problem with opSimulationManager. Simulation process failed to start.");
                        isRunning = false;
                        return;
                    }

                    newProcess->waitForFinished(-1);
        
                    if(newProcess->exitCode() != 0)
                    {
                        logging::ERROR("Simulation aborted. opSimulationManager returned with -1");
                        delete newProcess;
                        isRunning = false;
                        return;
                    }
                    else
                    {
                        logging::INFO("Simulation Manager Finished, Results Generated.");
                        delete newProcess;
                        isRunning = false;
                        return;
                    }

                    isRunning = false;
                    return;
                });
            
                resp.setResponse("Simulation started correctly");
                return true;
            }
            else
            {
                resp.setResponse("Simulation already running");
                logging::INFO("Op Simulation Manager is still running. Please wait for last simulation to exit.");
                return false;
            }
            
        }
        catch(QException exp)
        {
            logging::ERROR("Exception in api_run_opSimulationManager : " + std::string(exp.what()));
            return false;
        }
        catch(std::exception std_exp)
        {
            logging::ERROR("Exception in api_run_opSimulationManager : " + std::string(std_exp.what()));
            return false; 
        }
    }

    /*
        POST
        /exportOpsimulationManagerXml
        export valid data into the opSimulationManagerXml file
        
        Request :
        {
            "logLevel": 0,
            "logFileSimulationManager": "opSimulationManager.log",
            "simulation": "opSimulation",
            "libraries": "modules",
            "simulationConfig": {
                                    "numberOfExecutions": 1,
                                    "logFileSimulation": "opSimulation.log",
                                    "configurations": "configs",
                                    "results": "results"
                                }
        }

        Response :
        {
            "response": "opsimulation manager data exported correctly!"
        }     
    */
    bool api_export_opSimulationManager_xml(OpenAPI::OAIOpSimulationManagerXmlRequest req,OpenAPI::OAIDefault200Response &resp)
    {
        try
        {

            logging::INFO("Initialising : api_export_opSimulationManager_xml");
            OPGUIPCMSimulation::PCMSimulation sim;

            if(req.getSimulationConfigs().empty())
            {
                logging::ERROR("Internal Error, api_export_opSimulationManager_xml simulation configs are empty / invalid.");
                return false;
            }

            if(req.getLogLevel() < 0 || req.getLogLevel() > 5)
            {
                logging::ERROR("Internal Succes, api_export_opSimulationManager_xml log level is not invalid.");
                return false;
            }

            if(req.getLogFileSimulationManager().toStdString().empty() || req.getLogFileSimulationManager().toStdString() == "")
            {
                logging::ERROR("Internal Error, api_export_opSimulationManager_xml log file simulation manager is empty / invalid.");
                return false;
            }

            if(req.getLibraries().toStdString().empty() || req.getLibraries().toStdString() == "")
            {
                logging::ERROR("Internal Error, api_export_opSimulationManager_xml libraries path is empty / invalid.");
                return false;
            }

            if(req.getSimulation().toStdString().empty() || req.getSimulation().toStdString() == "")
            {
                logging::ERROR("Internal Error, api_export_opSimulationManager_xml opSimulation executable path is empty / invalid.");
                return false;
            }

            auto success = sim.GenerateSimulationXml(req.getSimulationConfigs(),req.getLogLevel(),req.getLogFileSimulationManager(),
            req.getLibraries(),req.getSimulation());

            if(success)
            {
                resp.setResponse("opsimulation manager data exported correctly!");
                logging::INFO("Xml file exported.");
                return true;
            }
        
            if(!success)
            {
                resp.setResponse("Xml file export failed");
                logging::INFO("Xml file export failed.");
                return true;
            }

        }
        catch(QException exp)
        {
            logging::ERROR("Exception in api_export_opSimulationManager_xml : " + std::string(exp.what()));
            return false;
        }
        catch(std::exception std_exp)
        {
            logging::ERROR("Exception in api_export_opSimulationManager_xml : " + std::string(std_exp.what()));
            return false; 
        }
    }
        

    /*
        POST
        /sendPCMFile
        send a PCM formatted file to the backend to be processed
        
        Request :
        {
            "path": "/pcmFile.db"
        }

        Response :
        {
            "response": "pcm file sent correctly"
        }    
    */

    bool api_send_PCM_file(OpenAPI::OAIPathRequest req, OpenAPI::OAIDefault200Response &resp)
    {
        try
        {
            logging::INFO("Initialising : api_send_PCM_file");

            if(sim != nullptr)
            {
                logging::INFO("Old Instance of PCMSimulation plugin found, deleting.");
                delete sim;
            }

            bool isInitialised = true;

            auto pcm_db_path = req.getPath();

            logging::INFO("Recieved path of PCM DB file : " + pcm_db_path.toStdString());
            
            if(pcm_db_path.isEmpty())
            {
                logging::ERROR("PCM DB file path is empty.");
                isInitialised = false;
            }

            auto pcm_FileExists = FileUtils::FileExists(pcm_db_path.toStdString());
            if(!pcm_FileExists)
            {
                logging::ERROR("PCM DB file path invalid.");
                isInitialised = false;
            }

            auto agent_car_1 = BackendConfig::getInstance().PATH_OPENPASS_CORE + "/" + 
                                                        BackendConfig::getInstance().PATH_AGENT_FOLLOW;
            auto agent_car_1_exists = FileUtils::FileExists(agent_car_1);
            if(!agent_car_1_exists)
            {
                logging::ERROR("Agent Car 1 file path invalid.");
                isInitialised = false;    
            }

            auto agent_car_2 = BackendConfig::getInstance().PATH_OPENPASS_CORE + "/" + 
                                                        BackendConfig::getInstance().PATH_AGENT_FOLLOW;
            auto agent_car_2_exists = FileUtils::FileExists(agent_car_2);
            if(!agent_car_2_exists)
            {
                logging::ERROR("Agent Car 2 file path invalid.");
                isInitialised = false;    
            }

            auto agent_car_other = BackendConfig::getInstance().PATH_OPENPASS_CORE + "/" + 
                                                        BackendConfig::getInstance().PATH_AGENT_COPY;
            auto agent_car_other_exists = FileUtils::FileExists(agent_car_other);
            if(!agent_car_other_exists)
            {
                logging::ERROR("Agent Car other file path invalid.");
                isInitialised = false;    
            }

            auto path_ModulesFolder = BackendConfig::getInstance().PATH_OPENPASS_CORE + "/" + BackendConfig::getInstance().PATH_MODULES;
            auto modules_exists = FileUtils::FolderExists(path_ModulesFolder);
            if(!modules_exists)
            {
                logging::ERROR("Modules folder path empty / invalid.");
                isInitialised = false;    
            }

            auto path_to_converted_cases = BackendConfig::getInstance().WORKSPACE;
            auto wksp_exists = FileUtils::FolderExists(path_to_converted_cases);
            if(!pathToConvertedCases.empty() || pathToConvertedCases != "")
            {
                if((FileUtils::FolderExistsInsideWorkspace(pathToConvertedCases,BackendConfig::getInstance().WORKSPACE)
                || pathToConvertedCases == BackendConfig::getInstance().WORKSPACE) && wksp_exists )
                {
                    path_to_converted_cases = pathToConvertedCases;
                    logging::INFO("Setting path to converted cases using path set by pathToConvertedCases Api as : " + pathToConvertedCases);
                }
                else
                {
                    logging::INFO("Using default path for saving converted cases as : " + path_to_converted_cases);
                } 
            }

            auto path_OpSimulation = BackendConfig::getInstance().PATH_OPENPASS_CORE + "/" + BackendConfig::getInstance().PATH_OP_SIMULATION;
            auto opSimulation_exists = FileUtils::FileExists(path_OpSimulation);
            if(!opSimulation_exists)
            {
                logging::ERROR("opSimulation file path empty / invalid.");
                isInitialised = false;    
            }
            
            auto path_OpSimulationManager = BackendConfig::getInstance().PATH_OPENPASS_CORE + "/" + BackendConfig::getInstance().PATH_OP_SIMULATION_MANAGER;
            auto opSimulationManager_exists = FileUtils::FileExists(path_OpSimulationManager);
            if(!opSimulationManager_exists)
            {
                logging::ERROR("opSimulationManager file path empty / invalid.");
                isInitialised = false;    
            }

            if(isInitialised)
            {
                QStringList agents_car_1;
                agents_car_1.append(QString::fromStdString(BackendConfig::getInstance().PATH_OPENPASS_CORE + "/" + 
                                                        BackendConfig::getInstance().PATH_AGENT_FOLLOW));
                QStringList agents_car_2;
                agents_car_2.append(QString::fromStdString(BackendConfig::getInstance().PATH_OPENPASS_CORE + "/" + 
                                                        BackendConfig::getInstance().PATH_AGENT_FOLLOW));
                QStringList agents_car_other;
                agents_car_other.append(QString::fromStdString(BackendConfig::getInstance().PATH_OPENPASS_CORE + "/" + 
                                                        BackendConfig::getInstance().PATH_AGENT_COPY));

                logging::INFO("Initialising PCM Simulation Plugin :");
                logging::INFO("With PCM DB Path : " + pcm_db_path.toStdString());
                logging::INFO("With Agents car 1 : " + agents_car_1.at(0).toStdString());
                logging::INFO("With Agents car 2 : " + agents_car_2.at(0).toStdString());
                logging::INFO("With Agents car other : " + agents_car_other.at(0).toStdString());
                logging::INFO("With Modules at : " + path_ModulesFolder);
                logging::INFO("With Path to Config at : "+ path_to_converted_cases);
                logging::INFO("With opSimulation at : " + path_OpSimulation);
                logging::INFO("With opSimulationManager at : " + path_OpSimulationManager);

                sim = new OPGUIPCMSimulation::PCMSimulation(pcm_db_path.toStdString(),
                                                            agents_car_1,
                                                            agents_car_2,
                                                            agents_car_other,
                                                            path_ModulesFolder,
                                                            path_to_converted_cases,
                                                            path_OpSimulation,
                                                            path_OpSimulationManager);

                QStringList pcm_case_list;
                auto success = sim->LoadCasesFromPcmFile(req.getPath(),pcm_case_list);

                if(success)
                {
            
                    QString pcm_db_cases = "[";
                    int counter = 0;
                    for(auto elem : pcm_case_list)
                    {
                        counter ++;
                        pcm_db_cases = pcm_db_cases + elem;
                        if(counter<pcm_case_list.size())
                        {
                            pcm_db_cases = pcm_db_cases + ",";
                        }
                    }
                    pcm_db_cases = pcm_db_cases + "]";
                
                    resp.setResponse(pcm_db_cases);
                    logging::INFO("PCM Cases found : " + pcm_db_cases.toStdString());
                    resp.setResponse("location setted correctly");

                    return true;
                }
                else
                {
                    resp.setResponse("[""null""]");
                    logging::ERROR("PCM DB Cases fetch failed.");
                    return false;
                }

            }
            else
            {
                logging::ERROR("Could not initialise PCM plugin properly.");
                return false;
            }

            
        }
        catch(QException exp)
        {
            logging::ERROR("Exception in api_send_PCM_file : " + std::string(exp.what()));
            return false;
        }
        catch(std::exception std_exp)
        {
            logging::ERROR("Exception in api_send_PCM_file : " + std::string(std_exp.what()));
            return false; 
        }

    }

        /*
        POST
        /pathToConvertedCases
        path to save the selected PCM converted cases
        
        Request :
        {
            "path": "/converted/PCM/selected/path"
        }

        Response :
        {
            "response": "location setted correctly"
        }   
    */
    bool api_path_to_converted_cases(OpenAPI::OAI_api_pathToConvertedCases_post_request req, OpenAPI::OAIDefault200Response &resp)
    {
        try
        {
            logging::INFO("Initialising : api_path_to_converted_cases");
            
            auto path_to_converted_cases = req.getPath();

            if(path_to_converted_cases.isEmpty())
            {
                logging::ERROR("Path given is empty.");
                resp.setResponse("Error in setting path of converted cases. Path is empty.");
                return false;
            }

            if(FileUtils::FolderExistsInsideWorkspace(path_to_converted_cases.toStdString(),BackendConfig::getInstance().WORKSPACE))
            {
                if(sim != nullptr)
                {
                    logging::INFO("PCM Plugin is already Initialised");
                    logging::INFO("Changing the path to converted cases for current instance.");
                    sim->SetPathToConvertedCases(path_to_converted_cases.toStdString());
                    pathToConvertedCases = path_to_converted_cases.toStdString();
                    logging::INFO("Setting path to converted cases as set by user.");
                    resp.setResponse("location setted correctly");
                    return true;
                }
                else
                {
                    pathToConvertedCases = path_to_converted_cases.toStdString();
                    logging::INFO("Setting path to converted cases as set by user.");
                    resp.setResponse("location setted correctly");
                    return true;
                }
            }
            else
            {
                logging::ERROR("Path given is outside workspace / incorrect / invalid.");
                resp.setResponse("Error in setting path of converted cases. Path is outside workspace / incorrect / invalid.");
                return false;
            }

        }
        catch(QException exp)
        {
            logging::ERROR("Exception in api_path_to_converted_cases : " + std::string(exp.what()));
            resp.setResponse("Error in setting path of converted cases.");
            return false;
        }
        catch(std::exception std_exp)
        {
            logging::ERROR("Exception in api_path_to_converted_cases : " + std::string(std_exp.what()));
            resp.setResponse("Error in setting path of converted cases.");
            return false; 
        }
        
    }

    
    
    /*
        POST
        /converToConfigs
        convert the selected PCM cases into configuration folders
        
        Request :
        {
            "selectedExperiments":  [
                                        1,
                                        2,
                                        3,
                                        4
                                    ]
        }

        Response :
        {
            "response": "location setted correctly"
        }   
    */
    bool api_convert_to_configs(OpenAPI::OAISelectedExperimentsRequest req, OpenAPI::OAIDefault200Response &resp)
    {
        try
        {
            logging::INFO("Initialising : api_convert_to_configs");

            auto experiments_for_config_conversion = req.getSelectedExperiments();

            if(experiments_for_config_conversion.empty() || experiments_for_config_conversion.isEmpty())
            {
                logging::ERROR("List of experiments for config conversion is invalid / empty.");
                return false;
            }

            if(sim != nullptr)
            {
                QStringList list_exp;
                logging::INFO("Generating configs for experiments :");
                for(auto experiment : experiments_for_config_conversion)
                {
                    QString s_exp = QString::number(experiment);
                    list_exp.append(s_exp);
                    logging::INFO(s_exp.toStdString());
                }
                
                auto results_pcm_config_conversion = sim->UIGenerateConfigOfPCMCases(list_exp);

                if(results_pcm_config_conversion.path_simulation_manager_config.empty())
                {
                    logging::ERROR("Config generation has failed.");
                    return false;
                }

                logging::INFO("Results of config generation stored in : " + results_pcm_config_conversion.path_config);
                logging::INFO("Config file opSimulationManager.xml stored in : " + results_pcm_config_conversion.path_simulation_manager_config);

                return true;
            }
            else
            {
                logging::ERROR("PCM Plugin is not initialised.");
                resp.setResponse("PCM Plugin is not initialised.");
                return false;
            }
        }
        catch(QException exp)
        {
            logging::ERROR("Exception in api_convert_to_configs : " + std::string(exp.what()));
            return false;
        }
        catch(std::exception std_exp)
        {
            logging::ERROR("Exception in api_convert_to_configs : " + std::string(std_exp.what()));
            return false; 
        }
    }


    /*
        POST
        /exportToSimulations
        execute pcm simulation manager and store the results
        
        Request :
        {
            "selectedExperiments":  [
                                        1,
                                        2,
                                        3,
                                        4
                                    ]
        }

        Response :
        {
            "response": "location setted correctly"
        }   
    */
    bool api_export_to_simulations(OpenAPI::OAISelectedExperimentsRequest req, OpenAPI::OAIDefault200Response &resp)
    {
        try
        {
            logging::INFO("Initialising : api_export_to_simulations");

            if(isRunning == false)
            {

                isRunning = true;

                auto experiments_for_export_to_simulations = req.getSelectedExperiments();

                if(experiments_for_export_to_simulations.empty() || experiments_for_export_to_simulations.isEmpty())
                {
                    logging::ERROR("List of experiments for export to simulations is invalid / empty.");
                    isRunning = false;
                    return false;
                }

                logging::INFO("Experiments haven been found.");

                if(sim != nullptr)
                {
                    QStringList list_exp;
                    logging::INFO("Generating configs for experiments :");
                
                    for(auto experiment : experiments_for_export_to_simulations)
                    {
                        QString s_exp = QString::number(experiment);
                        list_exp.append(s_exp);
                        logging::INFO(s_exp.toStdString());
                    }
                
                    auto results_pcm_config_conversion = sim->UIGenerateConfigOfPCMCases(list_exp);

                    if(results_pcm_config_conversion.path_simulation_manager_config.empty())
                    {
                        logging::ERROR("Config generation has failed.");
                        isRunning = false;
                        return false;
                    }

                    logging::INFO("Results of config generation stored in : " + results_pcm_config_conversion.path_config);
                    logging::INFO("Config file opSimulationManager.xml stored in : " + results_pcm_config_conversion.path_simulation_manager_config);

                    logging::INFO("Starting Simulation over selected experiments.");

                    // Check for opSimulationManager Path
                    QString path_core = QString::fromStdString(BackendConfig::getInstance().PATH_OPENPASS_CORE);
                    QString path_op_simulation_manager_exe = path_core + "/" + QString::fromStdString(BackendConfig::getInstance().OPSIMULATION_MANAGER_EXE);
                
                    if(!FileUtils::FileExists(path_op_simulation_manager_exe.toStdString()))
                    {   
                        logging::ERROR("The executable opSimulationManager is not found at path : " + path_op_simulation_manager_exe.toStdString());
                        isRunning = false;
                        return false;
                    }
                    else
                    {
                        logging::INFO("The executable opSimulationManager is found at path : " + path_op_simulation_manager_exe.toStdString());
                    }

                    // Check for opSimulation Path
                    QString path_op_simulation_exe = path_core + "/" + QString::fromStdString(BackendConfig::getInstance().OPSIMULATION_EXE);
                    if(!FileUtils::FileExists(path_op_simulation_exe.toStdString()))
                    {
                        logging::ERROR("The executable opSimulation is not found at path : " + path_op_simulation_exe.toStdString());
                        isRunning = false;
                        return false;
                    }
                    else
                    {
                        logging::INFO("The executable opSimulation is found at path : " + path_op_simulation_exe.toStdString());
                    }

                    QtConcurrent::run([path_op_simulation_manager_exe,results_pcm_config_conversion]()
                    {
                        QString path_op_sim_xml = QString::fromStdString(results_pcm_config_conversion.path_simulation_manager_config);

                        QProcess *mainProcess = new QProcess();
                        QStringList arguments;
                        arguments << "--config" << path_op_sim_xml;

                        mainProcess->start(path_op_simulation_manager_exe, arguments);
                        mainProcess->waitForFinished(-1);
                
                        if (mainProcess->exitCode() != 0)
                        {
                            logging::ERROR("Simulation aborted. opSimulationManager returned with -1");
                            delete mainProcess;
                            isRunning = false;
                            return;
                        }
                        
                        isRunning = false;
                        delete mainProcess;
                        return;
                    });

                    logging::INFO("Simulation Started.");
                    resp.setResponse("Simulation has started.");

                    return true;
                }
                else
                {
                    logging::ERROR("PCM Plugin is not initialised.");
                    resp.setResponse("PCM Plugin is not initialised.");
                    isRunning = false;
                    return false;
                }
        
            }
            else
            {
                logging::INFO("A simulation is still running");
                resp.setResponse("A simulation is still running");
                return false;
            }
        }
        catch(QException exp)
        {
            logging::ERROR("Exception in api_export_to_simulations : " + std::string(exp.what()));
            return false;
        }
        catch(std::exception std_exp)
        {
            logging::ERROR("Exception in api_export_to_simulations : " + std::string(std_exp.what()));
            return false; 
        }
    }
}


    
