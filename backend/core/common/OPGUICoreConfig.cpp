/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <OPGUICoreConfig.h>
#include <OPGUICoreConstants.h>
#include <FileUtils.h>
#include <OPGUILogger.h>

#include <QException>
#include <exception>

BackendConfig* BackendConfig::instance = nullptr;
std::once_flag BackendConfig::onceFlag;

BackendConfig& BackendConfig::getInstance()
{
    std::call_once(onceFlag, []() {
        instance = new BackendConfig();
        // Perform additional initialization if needed
        instance->SetAllValuesUsingConfigurationFile(BackendConstants::PATH_CONFIG_FILE());
    });
    return *instance;
}

BackendConfig::BackendConfig()
{
    logging::INFO("Path config file : " + BackendConstants::PATH_CONFIG_FILE());

    if(BackendConstants::PATH_CONFIG_FILE().empty())
    {
        logging::ERROR("Fatal Error, Path of config file is empty.");
    }

    if(!FileUtils::FileExists(BackendConstants::PATH_CONFIG_FILE()))
    {
        logging::ERROR("Fatal Error, Config file does not exist at given path : " + BackendConstants::PATH_CONFIG_FILE());
        logging::ERROR("Application will malfunction.");
    }

    PATH_OP_SIMULATION = "";
    PATH_OP_SIMULATION_MANAGER = "";
    PATH_MSYS = "";
    PATH_MINGW = "";
    PATH_APPLICATION_DIR = "";

    BACKEND_PORT = 0;
    BACKEND_IP = "";

    PATH_BASE_FOLDER = "";
    PATH_CONFIG_GEN_RESULT = "";
    PATH_DATABASE_PCM = "";
    PATH_AGENT_COPY = "";
    PATH_AGENT_FOLLOW = "";
    PATH_MODULES = "";
    LABEL_OP_SIMULATION_MANAGER_LOG = ""; 

    PATH_OPENPASS_CORE = "";
    OPSIMULATION_EXE = "";
    OPSIMULATION_MANAGER_XML = "";
    OPSIMULATION_MANAGER_EXE = "";
    WORKSPACE = "";
    PATH_HOME = "";

    auto res = SetAllValuesUsingConfigurationFile(BackendConstants::PATH_CONFIG_FILE());
}

BackendConfig::~BackendConfig()
{
    PATH_OP_SIMULATION = "";
    PATH_OP_SIMULATION_MANAGER = "";
    PATH_MSYS = "";
    PATH_MINGW = "";
    PATH_APPLICATION_DIR ="";

    BACKEND_PORT = 0;
    BACKEND_IP = "";

    PATH_BASE_FOLDER = "";
    PATH_CONFIG_GEN_RESULT = "";
    PATH_DATABASE_PCM = "";
    PATH_AGENT_COPY = "";
    PATH_AGENT_FOLLOW = "";
    PATH_MODULES = "";
    LABEL_OP_SIMULATION_MANAGER_LOG = ""; 

    PATH_OPENPASS_CORE = "";
    OPSIMULATION_EXE = "";
    OPSIMULATION_MANAGER_XML = "";
    OPSIMULATION_MANAGER_EXE = "";
    WORKSPACE = "";

    PATH_HOME = "";
}

auto 
BackendConfig::SetAllValuesUsingConfigurationFile(const std::string& configFilePath)->bool
{
    Reader readerObj;

    auto reader = readerObj.Read(configFilePath);

    if(reader.isEmpty())
    {
        logging::ERROR("Fatal Error during config initialisation.");
        logging::ERROR("Failure in setting config values, application will malfunction.");
        logging::INFO("Please check config path.");
        return false;
    }


    PATH_OP_SIMULATION = reader.value("PATH_OP_SIMULATION").toString().toStdString();
    PATH_OP_SIMULATION_MANAGER = reader.value("PATH_OP_SIMULATION_MANAGER").toString().toStdString();
    PATH_BASE_FOLDER = reader.value("PATH_BASE_FOLDER").toString().toStdString();
    PATH_MSYS = reader.value("PATH_MSYS").toString().toStdString();
    PATH_MINGW = reader.value("PATH_MINGW").toString().toStdString();
    BACKEND_PORT = reader.value("BACKEND_PORT").toInt();
    BACKEND_IP = reader.value("BACKEND_IP").toString().toStdString();
    PATH_BASE_FOLDER = reader.value("PATH_BASE_FOLDER").toString().toStdString();
    PATH_CONFIG_GEN_RESULT = reader.value("PATH_CONFIG_GEN_RESULT").toString().toStdString();
    PATH_DATABASE_PCM = reader.value("PATH_DATABASE_PCM").toString().toStdString();
    PATH_AGENT_COPY = reader.value("PATH_AGENT_COPY").toString().toStdString();
    PATH_AGENT_FOLLOW = reader.value("PATH_AGENT_FOLLOW").toString().toStdString();
    PATH_MODULES = reader.value("PATH_MODULES").toString().toStdString();
    PATH_COMPONENTS = reader.value("PATH_COMPONENTS").toString().toStdString();
    LABEL_OP_SIMULATION_MANAGER_LOG = reader.value("LABEL_OP_SIMULATION_MANAGER_LOG").toString().toStdString();
    PATH_OPENPASS_CORE = reader.value("PATH_OPENPASS_CORE").toString().toStdString();
    OPSIMULATION_EXE = reader.value("OPSIMULATION_EXE").toString().toStdString();
    OPSIMULATION_MANAGER_XML = reader.value("OPSIMULATION_MANAGER_XML").toString().toStdString();
    OPSIMULATION_MANAGER_EXE = reader.value("OPSIMULATION_MANAGER_EXE").toString().toStdString();
    WORKSPACE = reader.value("WORKSPACE").toString().toStdString();
    PATH_HOME = BackendConstants::PATH_HOME();

    if(PATH_OPENPASS_CORE=="")
    {
        this->PATH_OPENPASS_CORE = BackendConstants::PATH_OPENPASS_CORE();
        if(PATH_OPENPASS_CORE.empty())
        {
            logging::ERROR("PATH_OPENPASS_CORE variable is invalid / empty / not set using config.");
        }
    }    

    if(OPSIMULATION_EXE=="")
    {
        this->OPSIMULATION_EXE = BackendConstants::OPSIMULATION_EXE();
        if(OPSIMULATION_EXE.empty())
        {
            logging::ERROR("OPSIMULATION_EXE variable is invalid / empty / not set using config.");
        }
    }  

    if(OPSIMULATION_MANAGER_XML=="")
    {
        this->OPSIMULATION_MANAGER_XML = BackendConstants::OPSIMULATION_MANAGER_XML();
        if(OPSIMULATION_MANAGER_XML.empty())
        {
            logging::ERROR("OPSIMULATION_MANAGER_XML variable is invalid / empty / not set using config.");
        }
    }

    if(OPSIMULATION_MANAGER_EXE=="")
    {
        this->OPSIMULATION_MANAGER_EXE = BackendConstants::OPSIMULATION_MANAGER_EXE();
        if(OPSIMULATION_MANAGER_EXE.empty())
        {
            logging::ERROR("OPSIMULATION_MANAGER_EXE variable is invalid / empty / not set using config.");
        }
    }

    if(WORKSPACE=="")
    {
        this->WORKSPACE = BackendConstants::WORKSPACE();
        if(WORKSPACE.empty())
        {
            logging::ERROR("WORKSPACE variable is invalid / empty / not set using config.");
        }
    }
    
    if(PATH_OP_SIMULATION=="")
    {
        this->PATH_OP_SIMULATION = BackendConstants::PATH_OP_SIMULATION();
        if(PATH_OP_SIMULATION.empty())
        {
            logging::ERROR("PATH_OP_SIMULATION variable is invalid / empty / not set using config.");
        }
    }

    if(PATH_OP_SIMULATION_MANAGER=="")
    {
        this->PATH_OP_SIMULATION_MANAGER = BackendConstants::PATH_OP_SIMULATION_MANAGER();
        if(PATH_OP_SIMULATION_MANAGER.empty())
        {
            logging::ERROR("PATH_OP_SIMULATION_MANAGER variable is invalid / empty / not set using config.");
        }
    }

    if(PATH_MSYS=="")
    {
        this->PATH_MSYS = BackendConstants::PATH_MSYS();
        if(PATH_MSYS.empty())
        {
            logging::ERROR("PATH_MSYS variable is invalid / empty / not set using config.");
        }
    }

    if(PATH_MINGW=="")
    {
        this->PATH_MINGW = BackendConstants::PATH_MINGW();
        if(PATH_MINGW.empty())
        {
            logging::ERROR("PATH_MINGW variable is invalid / empty / not set using config.");
        }
    }

    if(BACKEND_PORT==NULL || BACKEND_PORT==0 )
    {
        this->BACKEND_PORT = BackendConstants::BACKEND_PORT();
        if(BACKEND_PORT == 0)
        {
            logging::ERROR("BACKEND_PORT variable is invalid / empty / not set using config.");
        }
    }

    if(BACKEND_IP=="")
    {
        this->BACKEND_IP = BackendConstants::BACKEND_IP();
        if(BACKEND_IP.empty())
        {
            logging::ERROR("BACKEND_IP variable is invalid / empty / not set using config.");
        }
    }

    if(PATH_BASE_FOLDER=="")
    {
        this->PATH_BASE_FOLDER = BackendConstants::PATH_BASE_FOLDER();
        if(PATH_BASE_FOLDER.empty())
        {
            logging::ERROR("PATH_BASE_FOLDER variable is invalid / empty / not set using config.");
        }
    }

    if(PATH_CONFIG_GEN_RESULT=="")
    {
        this->PATH_CONFIG_GEN_RESULT = BackendConstants::PATH_CONFIG_GEN_RESULT();
        if(PATH_CONFIG_GEN_RESULT.empty())
        {
            logging::ERROR("PATH_CONFIG_GEN_RESULT variable is invalid / empty / not set using config.");
        }
    }

    if(PATH_DATABASE_PCM=="")
    {
        this->PATH_DATABASE_PCM = BackendConstants::PATH_DATABASE_PCM();
        if(PATH_DATABASE_PCM.empty())
        {
            logging::ERROR("PATH_DATABASE_PCM variable is invalid / empty / not set using config.");
        }
    }

    if(PATH_AGENT_COPY=="")
    {
        this->PATH_AGENT_COPY = BackendConstants::PATH_AGENT_COPY();
        if(PATH_AGENT_COPY.empty())
        {
            logging::ERROR("PATH_AGENT_COPY variable is invalid / empty / not set using config.");
        }
    }

    if(PATH_AGENT_FOLLOW=="")
    {
        this->PATH_AGENT_FOLLOW = BackendConstants::PATH_AGENT_FOLLOW();
        if(PATH_AGENT_FOLLOW.empty())
        {
            logging::ERROR("PATH_AGENT_FOLLOW variable is invalid / empty / not set using config.");
        }
    }

    if(PATH_MODULES=="")
    {
        this->PATH_MODULES = BackendConstants::PATH_MODULES();
        if(PATH_MODULES.empty())
        {
            logging::ERROR("PATH_MODULES variable is invalid / empty / not set using config.");
        }
    }

    if(PATH_COMPONENTS=="")
    {
        this->PATH_COMPONENTS = BackendConstants::PATH_COMPONENTS();
        if(PATH_COMPONENTS.empty())
        {
            logging::ERROR("PATH_COMPONENTS variable is invalid / empty / not set using config.");
        }
    }

    if(LABEL_OP_SIMULATION_MANAGER_LOG=="")
    {
        this->LABEL_OP_SIMULATION_MANAGER_LOG = BackendConstants::LABEL_OP_SIMULATION_MANAGER_LOG();
        if(LABEL_OP_SIMULATION_MANAGER_LOG.empty())
        {
            logging::ERROR("LABEL_OP_SIMULATION_MANAGER_LOG variable is invalid / empty / not set using config.");
        }
    }

    
    return true;
    
}

bool BackendConfig::SetApplicationDirPath()
{
    //auto prefixPath = BackendUtils::exec("echo %USERPROFILE%");
    //this->PATH_APPLICATION_DIR = prefixPath + // prefix path  = C:\Users\Rishabh Srivastava
    //const std::string BackendConstants::PATH_CONFIG_FILE=add+std::string("\\opgui\\backend\\backendConfig.json");

    return true;
}

BackendConfigCleanup::~BackendConfigCleanup()
{
    delete BackendConfig::instance;
    BackendConfig::instance = nullptr;
}

auto 
Reader::Read(std::string FilePath)->QJsonObject 
{
    //try
    {

        QFile file(QString::fromStdString(FilePath));
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) 
        {
            qDebug() << "Failed to open file";
            //logging::ERROR("Failed to open file");
            //QJsonObject settings;
            //return settings;
        }
    
        QJsonDocument doc = QJsonDocument::fromJson(file.readAll());
        QJsonObject settings = doc.object(); 

        return settings;
    }
    
    // catch(QException exp)
    // {
    //     logging::ERROR("Exception in config init : " + std::string(exp.what()));
    //     QJsonObject settings;
    //     return settings;
    // }
    // catch(std::exception std_exp)
    // {
    //     logging::ERROR("Exception in config init : " + std::string(std_exp.what()));
    //     QJsonObject settings;
    //     return settings; 
    // }
    
}