/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#pragma once

#ifndef OPGUI_CORE_H
#define OPGUI_CORE_H

/* OAI REQUESTS */ 
#include <OAIPathRequest.h>
#include <OAIExportOpsimulationManagerXmlApiRequest.h>
#include <OAIOpSimulationManagerXmlRequest.h>
#include <OAISelectedExperimentsRequest.h>
#include <OAI_api_pathToConvertedCases_post_request.h>

/* OAI RESPONSES */
#include <OAIVerifyPath_200_response.h>
#include <OAIDefault200Response.h>
#include <OAIBoolean200Response.h>


namespace OPGUICore
{

    /*
        Checks if a simulation is in progress.
    */
    static bool isRunning = false;

    /*
        Stores path to converted cases.
    */
    static std::string pathToConvertedCases = "";

    /*
        POST
        /verify-path
        Verify if a path exists and has data
        
        Request :
        {
            "path": "/test"
        }

        Response :
        {
            "ok": true,
            "realPath": "os/workspace/path/to/folder/test1",
            "empty": false
        }       
    */
    bool api_verify_path(OpenAPI::OAIPathRequest req, OpenAPI::OAIVerifyPath_200_response &resp);
    


    /*
        POST
        /delete-information
        Detele information of given folder
        
        Request :
        {
            "path": "/os/workspace/path/to/file"
        }

        Response :
        {
            "ok": true
        }        
    */
    bool api_delete_information(OpenAPI::OAIPathRequest req, OpenAPI::OAIDefault200Response &resp);
    
    
    /* */
    bool api_is_simulation_running(OpenAPI::OAIBoolean200Response &resp);
    
    /*
        GET
        /runOpSimulationManager
        execute opSimulationManager
        
        Request :
            NULL

        Response :
        {
            "key": "value"
        }      
    */
    bool api_run_opSimulationManager(OpenAPI::OAIDefault200Response &resp);

    
    
    /*
        POST
        /exportOpsimulationManagerXml
        export valid data into the opSimulationManagerXml file
        
        Request :
        {
            "logLevel": 0,
            "logFileSimulationManager": "opSimulationManager.log",
            "simulation": "opSimulation",
            "libraries": "modules",
            "simulationConfig": {
                                    "numberOfExecutions": 1,
                                    "logFileSimulation": "opSimulation.log",
                                    "configurations": "configs",
                                    "results": "results"
                                }
        }

        Response :
        {
            "response": "opsimulation manager data exported correctly!"
        }     
    */
    bool api_export_opSimulationManager_xml(OpenAPI::OAIOpSimulationManagerXmlRequest req, OpenAPI::OAIDefault200Response &resp);



    /*
        POST
        /sendPCMFile
        send a PCM formatted file to the backend to be processed
        
        Request :
        {
            "path": "/pcmFile.db"
        }

        Response :
        {
            "response": "pcm file sent correctly"
        }    
    */
    bool api_send_PCM_file(OpenAPI::OAIPathRequest req, OpenAPI::OAIDefault200Response &resp);



    /*
        POST
        /pathToConvertedCases
        path to save the selected PCM converted cases
        
        Request :
        {
            "path": "/converted/PCM/selected/path"
        }

        Response :
        {
            "response": "location setted correctly"
        }   
    */
    bool api_path_to_converted_cases(OpenAPI::OAI_api_pathToConvertedCases_post_request req, OpenAPI::OAIDefault200Response &resp);

    
    
    /*
        POST
        /converToConfigs
        convert the selected PCM cases into configuration folders
        
        Request :
        {
            "selectedExperiments":  [
                                        1,
                                        2,
                                        3,
                                        4
                                    ]
        }

        Response :
        {
            "response": "location setted correctly"
        }   
    */
    bool api_convert_to_configs(OpenAPI::OAISelectedExperimentsRequest req, OpenAPI::OAIDefault200Response &resp);


    /*
        POST
        /exportToSimulations
        execute pcm simulation manager and store the results
        
        Request :
        {
            "selectedExperiments":  [
                                        1,
                                        2,
                                        3,
                                        4
                                    ]
        }

        Response :
        {
            "response": "location setted correctly"
        }   
    */
    bool api_export_to_simulations(OpenAPI::OAISelectedExperimentsRequest req, OpenAPI::OAIDefault200Response &resp);
}

#endif
