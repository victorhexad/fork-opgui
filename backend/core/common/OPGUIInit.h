/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#pragma once

#ifndef OPGUI_INIT_H
#define OPGUI_INIT_H

#include <OPGUILogger.h>

namespace OPGUIInit
{
    bool Initialise_OPGUI();
    void INFO_INITIALISED();
    void ERROR_NOT_INITIALISED();
} // namespace OPGUIInit



#endif