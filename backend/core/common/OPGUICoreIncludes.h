/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#pragma once

#ifndef OPGUI_CORE_INCLUDES_H
#define OPGUI_CORE_INCLUDES_H

#include <string>
#include <fstream>
#include <iostream>
#include <filesystem>
#include <vector>
#include <OPGUILogger.h>

#define EMPTY ""
#define ENUM_TO_STR(ENUM) std::string(#ENUM)

namespace BackendUtils
{
    std::string exec(const char* cmd);
}

#endif




