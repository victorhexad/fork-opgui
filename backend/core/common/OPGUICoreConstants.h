/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#pragma once

#ifndef OPGUI_CORE_CONSTANTS
#define OPGUI_CORE_CONSTANTS

#include <OPGUICoreIncludes.h>

class BackendConstants
{
public:
    BackendConstants()=default;
    ~BackendConstants()=default;

    // PATH VARIABLES
    static const std::string& PATH_OP_SIMULATION();
    static const std::string& PATH_OP_SIMULATION_MANAGER();
    static const std::string& PATH_BASE_FOLDER();
    static const std::string& PATH_MSYS();
    static const std::string& PATH_MINGW();
    static const std::string& PATH_CONFIG_FILE();
    static const std::string& PATH_COMPONENTS();
    static const std::string& PATH_MODULES();
    static const std::string& PATH_CONFIG_GEN_RESULT();
    static const std::string& PATH_DATABASE_PCM();
    static const std::string& PATH_AGENT_COPY();
    static const std::string& PATH_AGENT_FOLLOW();
    static const std::string& LABEL_OP_SIMULATION_MANAGER_LOG();

    static const std::string& PATH_OPENPASS_CORE();
    static const std::string& OPSIMULATION_EXE();
    static const std::string& OPSIMULATION_MANAGER_XML();
    static const std::string& OPSIMULATION_MANAGER_EXE();
    static const std::string& WORKSPACE();

    static int LOG_LEVEL_OPSIMULATIONMANAGER();
    static const std::string& PATH_OP_SIMULATION_MANAGER_XML();

    // BACKEND PORT
    static int BACKEND_PORT();
    static const std::string& BACKEND_IP();

    // BACKEND MESSAGES
    static const std::string& MESSAGE_SERVER_RUN_SUCCESS();
    static const std::string& MESSAGE_SERVER_RUN_FAIL();

    static const std::string& PATH_HOME();
};

#endif

