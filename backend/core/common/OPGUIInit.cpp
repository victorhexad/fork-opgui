/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include <OPGUIInit.h>
#include <OPGUICoreConfig.h>
#include <OPGUICore.h>
#include <FileUtils.h>

#include <QException>

#include <exception>

namespace OPGUIInit
{
    bool Initialise_OPGUI()
    {
        try
        {
            logging::INFO("Initialising opGUI");
            bool isInitialised = true;
        
            auto &config = BackendConfig::getInstance();

            // Check for Workspace DIR
            auto ifWkspDIRExists = FileUtils::FolderExists(config.WORKSPACE);
        
            // If not exist create Workspace DIR
            if(ifWkspDIRExists)
            {
                logging::INFO("Workspace DIR Found at : " + config.WORKSPACE);
                
            }
            else
            {
                logging::ERROR("Workspace DIR is not Found at : " + config.WORKSPACE);
                logging::INFO("Creating Workspace DIR : " + config.WORKSPACE);
                auto is_wksp_created = FileUtils::CreateFolder(config.WORKSPACE);

                if(is_wksp_created)
                {
                    logging::INFO("Created Workspace DIR : " + config.WORKSPACE);
                }
                else
                {
                    logging::ERROR("Failure in Creating Workspace DIR : " + config.WORKSPACE);
                    isInitialised = false;
                }
            }


            // Check for opSimulation DIR
            auto ifOpSimulationDIRExists = FileUtils::FolderExists(config.PATH_OPENPASS_CORE);

            if(ifOpSimulationDIRExists)
            {
                logging::INFO("OpSimulation DIR Found at : " + config.PATH_OPENPASS_CORE);
            }
            else
            {
                logging::ERROR("Fatal Error During Initialisation, OpSimulation DIR is not Found at : " + config.PATH_OPENPASS_CORE);
                isInitialised = false;
            }

            // Check for modules DIR
            std::string modules_dir = config.PATH_OPENPASS_CORE + "/" + config.PATH_MODULES;
            auto ifOpSimulationModulesExists = FileUtils::FolderExists(modules_dir);

            if(ifOpSimulationModulesExists)
            {
                logging::INFO("OpSimulation Modules DIR Found at : " + modules_dir);
            }
            else
            {
                logging::ERROR("Fatal Error During Initialisation, OpSimulation Modules DIR is not Found at : " + modules_dir);
                isInitialised = false;
            }

            // Check for commponents DIR
            std::string components_dir = config.PATH_OPENPASS_CORE + "/" + config.PATH_COMPONENTS;
            auto ifOpSimulationComponentsExists = FileUtils::FolderExists(components_dir);

            if(ifOpSimulationComponentsExists)
            {
                logging::INFO("OpSimulation Components DIR Found at : " + components_dir);
            }
            else
            {
                logging::ERROR("Fatal Error During Initialisation, OpSimulation Components DIR is not Found at : " + components_dir);
                isInitialised = false;
            }

            // Check for opSimulation executable
            std::string opSimulationExecutable = config.PATH_OPENPASS_CORE + "/" + config.PATH_OP_SIMULATION;
            auto ifOpSimulationExecutableExists = FileUtils::FileExists(opSimulationExecutable);

            if(ifOpSimulationExecutableExists)
            {
                logging::INFO("opSimulation executable Found at : " + opSimulationExecutable);
            }
            else
            {
                logging::ERROR("Fatal Error During Initialisation, OpSimulation Executable  is not Found at : " + opSimulationExecutable);
                isInitialised = false;
            }

            // Check for opSimulationManager executable
            std::string opSimulationManagerExecutable = config.PATH_OPENPASS_CORE + "/" + config.PATH_OP_SIMULATION_MANAGER;
            auto ifOpSimulationManagerExecutableExists = FileUtils::FileExists(opSimulationManagerExecutable);

            if(ifOpSimulationManagerExecutableExists)
            {
                logging::INFO("opSimulationManager executable Found at : " + opSimulationManagerExecutable);
            }
            else
            {
                logging::ERROR("Fatal Error During Initialisation, opSimulationManager executable is not Found at : " + opSimulationManagerExecutable);
                isInitialised = false;
            }
            
            OPGUICore::isRunning = false;

            if(isInitialised)
            {
                logging::INFO("Successfully initialised opGUI");
            }
            else
            {
                logging::ERROR("Failed to properly initialise opGUI, Application might not work correctly.");
            }

            return isInitialised;

        }
        catch(QException exp)
        {
            logging::ERROR("Exception in opGUI initialisation : " + std::string(exp.what()));
            return false;
        }
        catch(std::exception std_exp)
        {
            logging::ERROR("Exception in opGUI initialisation : " + std::string(std_exp.what()));
            return false; 
        } 
    }

    void INFO_INITIALISED()
    {
        logging::INFO("Application initialised successfully");
    }
        
    void ERROR_NOT_INITIALISED()
    {
        logging::ERROR("One or more errors have been encountered during initialisation, application might not work properly.");
    }
    
} // namespace OPGUIInit