/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#include "OPGUICoreConstants.h"

const std::string& BackendConstants::PATH_OP_SIMULATION() {
#ifdef _WIN32
        static const std::string path = "opSimulation.exe";
#elif __APPLE__ || __linux__
        static const std::string path = "opSimulation";
#endif
    return path;
}

const std::string& BackendConstants::PATH_OP_SIMULATION_MANAGER() {
#ifdef _WIN32
        static const std::string path = "opSimulationManager.exe";
#elif __APPLE__ || __linux__
        static const std::string path = "opSimulationManager";
#endif   
    return path;
}

const std::string& BackendConstants::PATH_BASE_FOLDER() {
    static const std::string path = "/opSimulation/bin/core";
    return path;
}

const std::string& BackendConstants::PATH_MSYS() {
    static const std::string path = "C:/msys64/usr/bin";
    return path;
}

const std::string& BackendConstants::PATH_MINGW() {
    static const std::string path = "C:/msys64/mingw64/bin";
    return path;
}

const std::string& BackendConstants::PATH_COMPONENTS() {
    static const std::string path = "components";
    return path;
}

const std::string& BackendConstants::PATH_MODULES() {
    static const std::string path = "modules";
    return path;
}

const std::string& BackendConstants::PATH_CONFIG_GEN_RESULT() {
    static const std::string path = "results";
    return path;
}

const std::string& BackendConstants::PATH_DATABASE_PCM() {
    static const std::string path = "/Documents/workspaces/opGUI-workspace1/";
    return path;
}

const std::string& BackendConstants::PATH_AGENT_COPY() {
    static const std::string path = "Agent_Copy.xml";
    return path;
}

const std::string& BackendConstants::PATH_AGENT_FOLLOW() {
    static const std::string path = "Agent_Follow.xml";
    return path;
}

const std::string& BackendConstants::LABEL_OP_SIMULATION_MANAGER_LOG() {
    static const std::string label = "opSimulationManager.log";
    return label;
}

int BackendConstants::LOG_LEVEL_OPSIMULATIONMANAGER() {
    static const int level = 5;
    return level;
}

const std::string& BackendConstants::PATH_OP_SIMULATION_MANAGER_XML() {
    static const std::string path = "opSimulationManager.xml";
    return path;
}

const std::string& BackendConstants::PATH_CONFIG_FILE() {
    
    static const std::string path = [] {
#ifdef _WIN32
        auto add = BackendUtils::exec("echo %USERPROFILE%");
        return add+"\\opgui\\backend\\backendConfig.json";
#elif __APPLE__ || __linux__
        auto add = BackendUtils::exec("echo $HOME");
        return add+"/opgui/backend/backendConfig.json";
#endif
        
    }();
    return path;
}

const std::string& BackendConstants::PATH_HOME() {
    static const std::string path = [] {
#ifdef _WIN32 
        return BackendUtils::exec("echo %USERPROFILE%");
#elif __APPLE__ || __linux__
        return BackendUtils::exec("echo $HOME");
#endif    
    }();
    return path;
}

int BackendConstants::BACKEND_PORT() {
    static const int port = 8848;
    return port;
}

const std::string& BackendConstants::BACKEND_IP() {
    static const std::string ip = "127.0.0.1";
    return ip;
}

const std::string& BackendConstants::MESSAGE_SERVER_RUN_SUCCESS() {
    static const std::string message = "SERVER RUNNING AT";
    return message;
}

const std::string& BackendConstants::MESSAGE_SERVER_RUN_FAIL() {
    static const std::string message = "SERVER RUNNING FAILED";
    return message;
}

const std::string& BackendConstants::PATH_OPENPASS_CORE() {
    static const std::string path = "";
    return path;
}

const std::string& BackendConstants::OPSIMULATION_EXE() {
#ifdef _WIN32
        static const std::string exe = "opSimulation.exe";
#elif __APPLE__ || __linux__
        static const std::string exe = "opSimulation";
#endif  
    return exe;
}

const std::string& BackendConstants::OPSIMULATION_MANAGER_XML() {
    static const std::string xml = "opSimulationManager.xml";
    return xml;
}

const std::string& BackendConstants::OPSIMULATION_MANAGER_EXE() {
#ifdef _WIN32
        static const std::string exe = "opSimulationManager.exe";
#elif __APPLE__ || __linux__
        static const std::string exe = "opSimulationManager";
#endif  
    return exe;
}

const std::string& BackendConstants::WORKSPACE() {
    static const std::string workspace = "";
    return workspace;
}
