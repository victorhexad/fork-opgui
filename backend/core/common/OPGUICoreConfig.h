/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

#pragma once

#ifndef OPGUI_CORE_CONFIG
#define OPGUI_CORE_CONFIG

#include <OPGUICoreIncludes.h>
#include <QJsonValue>
#include <QJsonDocument>
#include <QJsonObject>
#include <QFile>

#include <string>
#include <mutex>
#include <atomic>

class BackendConfig
{
    public:
    static BackendConfig& getInstance();

    BackendConfig(const BackendConfig& obj) = delete;
    BackendConfig& operator=(const BackendConfig& obj) = delete;
    BackendConfig(BackendConfig&& obj) = delete;
    BackendConfig& operator=(BackendConfig&& obj) = delete;

    bool SetAllValuesUsingConfigurationFile(const std::string& configFilePath);
    bool SetApplicationDirPath();

    std::string PATH_OPENPASS_CORE;
    std::string OPSIMULATION_EXE;
    std::string OPSIMULATION_MANAGER_XML;
    std::string OPSIMULATION_MANAGER_EXE;
    std::string WORKSPACE;
    
    // PATH VARIABLES
    std::string PATH_OP_SIMULATION;
    std::string PATH_OP_SIMULATION_MANAGER;
    std::string PATH_MSYS;
    std::string PATH_MINGW;
    std::string PATH_APPLICATION_DIR;

    // BACKEND PORT
    int BACKEND_PORT;
    std::string BACKEND_IP;

    std::string PATH_BASE_FOLDER;
    std::string PATH_CONFIG_GEN_RESULT;
    std::string PATH_DATABASE_PCM;
    std::string PATH_AGENT_COPY;
    std::string PATH_AGENT_FOLLOW;
    std::string PATH_MODULES;
    std::string PATH_COMPONENTS;
    std::string LABEL_OP_SIMULATION_MANAGER_LOG;
    std::string PATH_HOME;

    private:
    BackendConfig();
    ~BackendConfig();

    static BackendConfig* instance;
    static std::once_flag onceFlag;

    friend class BackendConfigCleanup; 

};

class BackendConfigCleanup
{
public:
    ~BackendConfigCleanup();
};

class Reader
{
    public:
    Reader()=default;
    ~Reader()=default;

    auto Read(std::string FilePath)->QJsonObject;
};

#endif