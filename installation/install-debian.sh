#!/bin/bash

OPGUI=$HOME/opgui
echo $OPGUI

echo [PROGRESS]  Backend Install INIT

cd $OPGUI/backend
echo $OPGUI/backend

mkdir build
cd build
cmake -DWITH_TESTS=OFF -DWITH_DOC=ON ..
make

cd $OPGUI/frontend
echo $OPGUI/frontend

yarn install && yarn build && cp -r dist ../backend/build 