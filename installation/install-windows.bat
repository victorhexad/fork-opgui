@echo off

SET OPGUI=%USERPROFILE%\opgui
echo %OPGUI%

:: Prerequisites -> node installation , path variables for msys
echo [PROGRESS]  Backend Install INIT

cd %OPGUI%
echo %OPGUI%
cd %OPGUI%\backend
echo %OPGUI%\backend

mkdir build
cd build
cmake -G "MinGW Makefiles" -DWITH_TESTS=OFF -DWITH_DOC=ON ..
cmake --build .

cd %OPGUI%\frontend
echo %OPGUI%\frontend

yarn install && yarn build & cp -r dist ..\backend\build

