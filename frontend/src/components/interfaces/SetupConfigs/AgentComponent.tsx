import { Box, Grid, Typography, Checkbox, useTheme } from "@mui/material";
import UploadFile from "../../commonComponents/UploadInput/UploadFile";
import { IAgent } from "./setupConfigs";
const AgentComponent = ({ name, path, index }: IAgent) => {
    const theme = useTheme();
    return (
        <Grid container sx={{ background: index % 2 === 0 ? theme.palette.common.white: `${theme.palette.primary.accent1}20`, borderBottom: `1px solid ${theme.palette.greyScale.main}`, ':last-of-type': { borderBottom: 'unset' }, paddingX: 4, paddingY: 2, width: '100%' }}>
            <Grid lg={4} md={4} sm={4} item sx={{ display: 'flex', flexDirection: 'column' }}>
                <Box display='flex' flex={1} gap={2} alignItems='center'>
                    <Checkbox color="primary" checked={true} />
                    <Typography>
                        {name}
                    </Typography>
                </Box>
            </Grid>
            <Grid lg={8} md={8} sm={8} item sx={{ display: 'flex', flexDirection: 'column' }}>
                <Box display='flex' flex={1} gap={2}>
                    <Box marginLeft={4} sx={{ display: 'flex', alignItems: 'center', width: '95%' }}>
                        <UploadFile route={path} error={path === ''} styles={{ flex: 1 }} name='agentFile' handleFiles={() => { }} />
                    </Box>
                </Box>
            </Grid>
        </Grid>
    )
}

export default AgentComponent;