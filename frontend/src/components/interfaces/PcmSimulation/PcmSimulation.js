/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import React from "react";
import {Box, FormControl, FormGroup, Typography} from "@mui/material";
import InputSection from "./InputSection/InputSection";
import OutputSection from "./OutputSection/OutputSection";
import SystemConfigurationSection from "./SystemConfigurationSection/SystemConfigurationSection";
import VariationSection from "./VariationSection/VariationSection";
import TopButtons from "./TopButtons/TopButtons";
import LinearProgress from "@mui/material/LinearProgress";

const PcmSimulation = () => {

    return(
        <Box>
            <TopButtons />
            <div className={'pcm-window'}>
                <div className={'side-window'}>
                    {/*side blank div*/}
                    <div><p>TO DO</p></div>
                    {/*main window*/}
                    <div id={'launch-bar'}>
                        <Box sx={{ display: 'flex', alignItems: 'center' }}>
                            <Box sx={{ width: '90%', mr: 1 }}>
                                <LinearProgress />
                                {/*<LinearProgress variant={'determinate'}/>*/}
                            </Box>
                            <Box sx={{ minWidth: 35 }}>
                                <Typography variant="body2" color="text.secondary">
                                    XXX%
                                </Typography>
                            </Box>
                        </Box>
                    </div>
                </div>
                <div className={'main-window'}>
                    <FormControl >
                    {/*<FormControl className={'main-window'}>*/}
                        <FormGroup>
                            <InputSection />
                        </FormGroup>
                        <FormGroup>
                            <OutputSection />
                        </FormGroup>
                        <FormGroup>
                            <SystemConfigurationSection />
                        </FormGroup>
                        <FormGroup>
                            <VariationSection />
                        </FormGroup>
                    </FormControl>
                </div>
            </div>
        </Box>
    )
}
export default PcmSimulation;