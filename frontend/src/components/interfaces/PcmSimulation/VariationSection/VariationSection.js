/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import React from "react";
import {Box, Checkbox, FormControlLabel, FormLabel, Grid, Radio, RadioGroup, Typography} from "@mui/material";
import NumericInput from "../../../commonComponents/NumericInput/NumericInput";
import '../PcmSimulation.css';
const VariationSection = () => {

    return (
        <Box className={'form-box'}>
            <Typography id={'variations'}>
                VARIATIONS
            </Typography>
            <Grid container columnSpacing={1} rowSpacing={2} >
                <Grid container item xs={12} alignItems={'self-start'}>
                    <Grid container item xs={12} alignItems={'self-start'}>
                        <Grid item xs={12}>
                            <FormLabel  htmlFor={"seed-select"}>Random Seed: </FormLabel>
                        </Grid>
                        <Grid item xs={3} sx={{display: 'contents'}}>
                            <RadioGroup row defaultValue={'case-number'} id={'seed-select'}>
                                <FormControlLabel value={'case-number'} control={<Radio sx={{height: 10}}/>} label={'use case number'} />
                                <FormControlLabel value={'given-value'} control={<Radio sx={{height: 10}}/>} label={'use given value'} />
                            </RadioGroup>
                        </Grid>
                        <Grid item xs={9}sx={{display: 'contents'}}>
                            <NumericInput
                                tagId={'mark-value-label'}
                                tag={'Value'}/>
                            <NumericInput
                                tagId={'mark-variation-label'}
                                tag={'Variation (int % ?)'}/>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={12}>
                    <Box className={'form-box'}>
                    {/*<Box className={'form-box'}>*/}
                        <FormControlLabel id={'trajectory-checking'} control={<Checkbox defaultChecked />} label="Trajectory shifting" />
                        <NumericInput
                        tagId={'trajectory-shifting-car1-label'}
                        tag={'Shift radius of the Car1 (m)'}/>
                        <NumericInput
                            tagId={'trajectory-shifting-car2-label'}
                            tag={'Shift radius of the Car2 (m)'}/>
                    </Box>
                </Grid>
                <Grid item xs={12}>
                    <Box className={'form-box'}>
                        <FormControlLabel id={'velocity-scaling'} control={<Checkbox defaultChecked />} label="Velocity Scaling" />
                        <NumericInput
                            tagId={'velocity-scaling-car1-label'}
                            tag={'Velocity scaling of the Car1 (m)'}/>
                        <NumericInput
                            tagId={'velocity-scaling-car2-label'}
                            tag={'Velocity scaling of the Car2 (m)'}/>
                    </Box>
                </Grid>
            </Grid>
        </Box>
    )
}
export default VariationSection;