/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import React from "react";
import {Box, FormLabel, Grid, MenuItem, TextField, Typography} from "@mui/material";
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import UploadInput from "../../../commonComponents/UploadInput/UploadInput";

const OutputSection = () => {
    const [logLevel, setLogLevel] = React.useState('info');

    return (
        <Box className={'form-box'}>
            <Typography id={'output'} gutterBottom>
                OUTPUT
            </Typography>
            <Grid container spacing={2} alignItems={'flex-start'}>
                <Grid item sm={4}>
                    <FormLabel htmlFor={"select-label"}>Log Level: </FormLabel>
                    <TextField
                        select
                        size={'small'}
                        sx={{ height: 25 }}
                        endicon={<KeyboardArrowDownIcon />}
                        labelid="select-label"
                        id="simple-select"
                        value={logLevel}
                        label="Log level"
                        onChange={(event) => setLogLevel(
                            event.target.value
                        )}
                    >
                        <MenuItem value={"error"}>Error</MenuItem>
                        <MenuItem value={"warning"}>Warning</MenuItem>
                        <MenuItem value={"info"}>Info</MenuItem>
                        <MenuItem value={"debug"}>Debug</MenuItem>
                    </TextField>
                </Grid>
                <Grid item sm={8}>
                    <FormLabel htmlFor={"file-path"}>Result folder: </FormLabel>
                    <UploadInput
                        inputKey={'file-path'}
                        id="file-path"
                        name="file-path" />
                </Grid>
            </Grid>
        </Box>
    )
}

export default OutputSection;