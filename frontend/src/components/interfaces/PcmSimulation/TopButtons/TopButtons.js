/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import React from "react";
import Tab from "@mui/material/Tab";
import Tabs from "@mui/material/Tabs";
import './TopButtons.css';

const TopButtons = () => {
    // use this URL only for dev purposes
    //const endpointUrl = 'https://55804cf6-dbc9-471a-a4b5-3f7c05bc6ff7.mock.pstmn.io/simulate/';
    const endpointUrl = 'http://127.0.0.1:8848/simulate/';
    const buttonNames = ['launch opSimulation',
        'Start simulation', 'Stop Simulation', 'Save Experiment', 'Load Experiment'];

    function executeOpSim() {
        fetch(endpointUrl, {
            method: 'GET',
        }).then((response) => {
            console.log('GET OK!')
            // DEV working json response from postman tests
            //const resp = response.json()
            const resp = response.text()
            
            console.log(resp);
        }).catch( (error) => {
                console.log(error);
        })
    }

    const navButtonNames = buttonNames.map( _ => {
            // Mocked POC Lunch sim button
            if (_ === 'launch opSimulation' || _ === 'Start simulation') {
                return <Tab disabled={false} key={_} label={_} value={_} onClick={executeOpSim} className={'topButton'}>
                </Tab>
            } else {
                return <Tab disabled={true} key={_} label={_} value={_} className={'topButton'}></Tab>
        }
    }
    );

    return(
        <Tabs value={false}>
            {navButtonNames}
        </Tabs>
    )
}
export default TopButtons;