/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import React from "react";
import {Box, FormControlLabel, FormLabel, Grid, Radio, RadioGroup, Typography} from "@mui/material";
import '../PcmSimulation.css';
import UploadInput from "../../../commonComponents/UploadInput/UploadInput";

const InputSection = () => {
    return (
        <Box className={'form-box'}>
            <Typography
                sx={{marginBottom: 0}}
                id={'input'} gutterBottom>
                INPUT
            </Typography>
            <Grid container spacing={2} alignItems={'flex-start'}>
                {/*seems to be config folder*/}
                <Grid item sm={4} sx={{alignItems: 'flex-start' }}>
                    <RadioGroup row defaultValue={'pcm-database'}>
                        <FormControlLabel value={'pcm-database'} control={<Radio sx={{height: 10}} />} label={'PCM Database'} />
                        <FormControlLabel value={'sim-results'} control={<Radio sx={{height: 10}}/>} label={'Simulation Results'} />
                    </RadioGroup>
                </Grid>
                <Grid item sm={8}>
                    <FormLabel htmlFor={"file-upload"} >PCM file: </FormLabel>
                    <UploadInput
                        inputKey={'file-upload'}
                        id="file-upload"
                        name="file-upload" />
                </Grid>
            </Grid>
        </Box>
    )
}
            // {/*Upload an folder alternative*/}
            // {/*<input*/}
            // {/*    id="file-upload"*/}
            // {/*    name="file-upload"*/}
            // {/*    type="file"*/}
            // {/*    directory={""}*/}
            // {/*    webkitdirectory={""}*/}
            // {/*    multiple={true}*/}
            // {/*    className="absolute inset-0 z-20 w-full pt-0 m-0 outline-none opacity-0 cursor-pointer"*/}
            // {/*/>*/}
export default InputSection;