/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import React from "react";
import {Box, FormLabel, Grid, Typography} from "@mui/material";
import UploadInput from "../../../commonComponents/UploadInput/UploadInput";

const SystemConfigurationSection = () => {

    const gridItemNames = [{path: 'car1-path',name: 'Car1:'},
        {path: 'car2-path', name: 'Car2:'}, {path: 'other', name: 'Other:'}];

    const gridInput = gridItemNames.map( gridElem =>
        <Grid container item columnSpacing={5} alignItems={'flex-start'} key={gridElem.path}>
            <Grid item xs={1}>
                <FormLabel htmlFor={gridElem.path}>{gridElem.name}</FormLabel>
            </Grid>
            <Grid item xs={11}>
                <UploadInput
                    inputKey={gridElem.path}
                    id={gridElem.path}
                    name={gridElem.path} />
            </Grid>
        </Grid>
    );

    return (
        <Box className={'form-box'}>
            <Typography id={'system-config-files'}>
                SYSTEM CONFIGURATION FILES
            </Typography>
            <Grid container columnSpacing={1} rowSpacing={2}>
                <Grid container item columnSpacing={4} alignItems={'flex-start'}>
                    {gridInput}
                </Grid>
            </Grid>
        </Box>
    )
}

export default SystemConfigurationSection;