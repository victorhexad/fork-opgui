/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */

import React from "react";
import {Box, FormControl, Input, Typography} from "@mui/material";

const NumericInput = (props) => {
    const [value, setValue] = React.useState(10);

    return (
        <Box sx={{display: 'flex'}}>
            <Typography id={props.tagId}>
                {props.tag}
            </Typography>
            <FormControl>
                <Input sx={{marginLeft: 2 , marginRight: 1,width: 30 }}
                       value={value}
                       size="small"
                       onChange={(event) => setValue(
                           Number(event.target.value)
                       )}
                       inputProps={{
                           step: 1,
                           min: 0,
                           max: 100,
                           type: 'number',
                       }}
                />
            </FormControl>
        </Box>
    )
}

export default NumericInput;