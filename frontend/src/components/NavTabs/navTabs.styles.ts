import { Tab, styled } from "@mui/material";

export const TabStyled = styled(Tab)({
    // textTransform: 'none',
    alignItems: 'flex-start',
    color: 'var(--primary-white)',
    cursor: 'pointer',
    fontWeight: 'bold',
    opacity: 1,
    '&.Mui-selected': {
        background: 'white',
        borderRadius: 2
    },
    '&.Mui-selected::after' : {
        position: 'absolute',
        content: "'ᐳ'",
        right: 20
    }
});
