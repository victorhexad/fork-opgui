/*
 * Copyright (c) 2023 Hexad GmbH
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 */
import {
    Configuration,
    VerifyPathApi,
    ExportOpsimulationManagerXmlApi,
    OpSimulationManagerXmlRequest,
    RunOpSimulationManagerApi,
    DeleteInformationApi,
    IsSimulationRunningApi
} from 'opGUI-client-frontend';
import { VITE_API_DEV_URL, VITE_API_LOCAL_URL, VITE_ENVIRONMENT } from './constants';
const environment = VITE_ENVIRONMENT;
const configuration = new Configuration({
    basePath: environment === 'dev' ? VITE_API_DEV_URL : VITE_API_LOCAL_URL
});
const verifyPathApi = new VerifyPathApi(configuration);
const exportOpsimulationManagerXmlApi = new ExportOpsimulationManagerXmlApi(configuration);
const runOpSimulationManagerApi = new RunOpSimulationManagerApi(configuration);
const deleteInformationApi = new DeleteInformationApi(configuration);
const isSimulationRunningApi = new IsSimulationRunningApi(configuration);

export const isSimRunning = async () => {
    try {
        const res = await isSimulationRunningApi.apiIsSimulationRunningGet();
        return res.response;
    } catch (error) {
        console.log(error);
    }

}
export const verifyPath = async (path: string) => {
    try {
        const res = await verifyPathApi.verifyPath({ pathRequest: { path: path } });
        return res;
    } catch (e) {
        console.log(e);
    }
};

export const exportXml = async (data: OpSimulationManagerXmlRequest) => {
    try {
        const res = await exportOpsimulationManagerXmlApi.apiExportOpsimulationManagerXmlPost({ opSimulationManagerXmlRequest: data })
        return res;
    } catch (e) {
        console.error(e);
    }
}

export const runSimulationManager = async () => {
    try {
        const res = await runOpSimulationManagerApi.apiRunOpSimulationManagerGet();
        console.log('Execution opSimManager OK!')
        return res.response;
    } catch (error) {
        console.log('Execution opSimManager Failed!')
    }
}

export const deleteInformation = async (path: string) => {
    try {
        const res = await deleteInformationApi.deleteInformation({ pathRequest: { path: path } });
        return res;
    } catch (error) {
        console.log(error);

    }
}
