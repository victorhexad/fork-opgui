/**
 * opGUI - OpenAPI 3.0
 * Dummy description about the available endpoints Some useful links: - [The opGUI repository](https://gitlab.eclipse.org/eclipse/openpass/opgui) - [The documentation page](https://www.eclipse.org/openpass/content/html/index.html)
 *
 * The version of the OpenAPI document: 1.1.0
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface Error404AllOf
 */
export interface Error404AllOf {
    /**
     *
     * @type {string}
     * @memberof Error404AllOf
     */
    message: string;
}
/**
 * Check if a given object implements the Error404AllOf interface.
 */
export declare function instanceOfError404AllOf(value: object): boolean;
export declare function Error404AllOfFromJSON(json: any): Error404AllOf;
export declare function Error404AllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): Error404AllOf;
export declare function Error404AllOfToJSON(value?: Error404AllOf | null): any;
