"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * opGUI - OpenAPI 3.0
 * Dummy description about the available endpoints Some useful links: - [The opGUI repository](https://gitlab.eclipse.org/eclipse/openpass/opgui) - [The documentation page](https://www.eclipse.org/openpass/content/html/index.html)
 *
 * The version of the OpenAPI document: 1.1.0
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.Error400AllOfToJSON = exports.Error400AllOfFromJSONTyped = exports.Error400AllOfFromJSON = exports.instanceOfError400AllOf = void 0;
/**
 * Check if a given object implements the Error400AllOf interface.
 */
function instanceOfError400AllOf(value) {
    var isInstance = true;
    isInstance = isInstance && "message" in value;
    return isInstance;
}
exports.instanceOfError400AllOf = instanceOfError400AllOf;
function Error400AllOfFromJSON(json) {
    return Error400AllOfFromJSONTyped(json, false);
}
exports.Error400AllOfFromJSON = Error400AllOfFromJSON;
function Error400AllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'message': json['message'],
    };
}
exports.Error400AllOfFromJSONTyped = Error400AllOfFromJSONTyped;
function Error400AllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'message': value.message,
    };
}
exports.Error400AllOfToJSON = Error400AllOfToJSON;
